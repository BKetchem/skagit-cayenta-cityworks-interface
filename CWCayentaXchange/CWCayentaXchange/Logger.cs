﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using log4net.Repository.Hierarchy;
using log4net.Appender;

namespace CWCayentaXchange
{
    public static class Logger
    {
        public static readonly ILog log = LogManager.GetLogger(typeof(CWCayentaXchange.Program));

        public static void InitLogger()
        {
            log4net.Config.XmlConfigurator.Configure();
            var WorkOrderAppender = ((Hierarchy)LogManager.GetRepository()).Root.Appenders.OfType<FileAppender>().FirstOrDefault();
            string WorkOrderAppenderFilename = WorkOrderAppender != null ? WorkOrderAppender.File : string.Empty;
        }
    }
}
