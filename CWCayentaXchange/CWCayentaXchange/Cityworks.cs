﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Configuration;

namespace CWCayentaXchange
{
    public static class CityworksFunc
    {
        public static bool UpdateCINT_CAYENTA_STATUS(XmlDocument cayentaReply, CWCayentaXchange.EEF.CINT_CAYENTA_STATUS wo)
        {
            //Return false if there's a problem
            bool returnValue = true;

            //Here for updating staging table
            XmlNodeList Reply = cayentaReply.SelectNodes("Reply");
            XmlNodeList AddWorkRequest = Reply[0].SelectNodes("AddWorkRequest");
            //XmlNodeList STATUS = AddWorkRequest[0].SelectNodes("STATUS"); // DEE - communication status
            XmlNodeList Params = AddWorkRequest[0].SelectNodes("Params");
            XmlNodeList STATUS = Params[0].SelectNodes("STATUS"); //DEE - request status
            XmlNodeList STATUS_DESC = Params[0].SelectNodes("STATUS_DESC"); //DEE - request status

            if (STATUS[0].InnerText == "0")
            {
                Console.WriteLine("AAAAAAAAAAAAAAAAAAAAAAAAAAAA");
                wo.PROCESSEDTOCAYENTA = true;
                wo.PROCESSEDTOCAYENTADATE = DateTime.Now;
                wo.COMMENT = Params[0].SelectSingleNode("STATUS_DESC").InnerText;
                wo.CAYENTAWONUMBER = Convert.ToDecimal(Params[0].SelectSingleNode("WP_NUMBER").InnerText);
                wo.CAYENTAWOPREFIX = Params[0].SelectSingleNode("WP_PREFIX").InnerText;

                returnValue = true;
            }
            else
            {
                Console.WriteLine("BBBBBBBBBBBBBBBBBBBBBBBBBBB");
                wo.PROCESSEDTOCAYENTA = false;
                if (Params[0].SelectSingleNode("STATUS_DESC").InnerText != "")
                {
                    wo.ERROR = Params[0].SelectSingleNode("STATUS_DESC").InnerText;
                }

                returnValue = false;
            }
                    
            return returnValue;
        }

         
        public static int LogError(String v_procedure, String v_woId, String v_error, String v_rt, String v_rId)
        {
            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            EEF.CINT_CAYENTA_ERRORS newError = new EEF.CINT_CAYENTA_ERRORS();

            newError.INTEGRATION_PROCEDURE = v_procedure;
            newError.DATETIME = DateTime.Now;
            newError.WO_ID = v_woId;
            newError.ERROR = v_error;
            newError.RESOURCE_TYPE = v_rt;
            newError.RESOURCE_ID = v_rId;
            
            cwEntities.CINT_CAYENTA_ERRORSs.AddObject(newError);
            return cwEntities.SaveChanges();
        }

        public static string GetPriorityDesciption(string priority)
        {
            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.PWCODE> cityworksWork = from pwCode in cwEntities.PWCODEs
                                                                                where pwCode.CODETYPE == "APRIORIT" &&
                                                                                pwCode.CODE == priority
                                                                                select pwCode;
            
            if (cityworksWork.Count() == 1) { return cityworksWork.First().DESCRIPTION; }
            else { return "MD"; }
        }

        public static int GetCityworksWorkCount()
        {
            return GetCityworksWork().Count();
        }

        public static System.Linq.IQueryable<CWCayentaXchange.EEF.CINT_CAYENTA_STATUS> GetCityworksWork()
        {
            
            //get all work orders regardless of update status that have not been posted to cayenta yet does not matter what their update status is as all info is passed
            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.CINT_CAYENTA_STATUS> query = from newWorkOrders in cwEntities.CINT_CAYENTA_STATUSs
                                                                                     where newWorkOrders.PROCESSEDTOCAYENTA == false 
                                                                                     && newWorkOrders.VALIDPROJECT == true //DEE - Added Valid Project
                                                                                     //&& (newWorkOrders.CWSTATUS != "CLOSED" || newWorkOrders.CWSTATUS != "DELETED")
                                                                                     && newWorkOrders.CWSTATUS != "CLOSED"
                                                                                     && newWorkOrders.CWSTATUS != "DELETED"
                                                                                     select newWorkOrders;

            return query;
        }

        public static System.Linq.IQueryable<CWCayentaXchange.EEF.CINT_CAYENTA_STATUS> GetCityworksNotClosed()
        {

            //get all work orders that have been processed to cayenta and are not closed
            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.CINT_CAYENTA_STATUS> query = from newWorkOrders in cwEntities.CINT_CAYENTA_STATUSs
                                                                                     where newWorkOrders.PROCESSEDTOCAYENTA == true
                                                                                     && newWorkOrders.VALIDPROJECT == true //DEE - Added Valid Project
                                                                                     //&& (newWorkOrders.CWSTATUS != "CLOSED" || newWorkOrders.CWSTATUS != "DELETED")
                                                                                     && newWorkOrders.CWSTATUS != "CLOSED"
                                                                                     && newWorkOrders.CWSTATUS != "DELETED"
                                                                                     select newWorkOrders;

            return query;
        }

        public static System.Linq.IQueryable<CWCayentaXchange.EEF.CINT_CAYENTA_STATUS> GetCityworksUpdates()
        {
            //get all work orders that have been processed to cayenta but have their update flag as true
            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.CINT_CAYENTA_STATUS> query = from newWorkOrders in cwEntities.CINT_CAYENTA_STATUSs
                                                                                     where newWorkOrders.PROCESSEDTOCAYENTA == true 
                                                                                     && newWorkOrders.UPDATETOCAY == true
                                                                                     //&& (newWorkOrders.CWSTATUS != "CLOSED" || newWorkOrders.CWSTATUS != "DELETED")
                                                                                     && newWorkOrders.CWSTATUS != "CLOSED"
                                                                                     && newWorkOrders.CWSTATUS != "DELETED"
                                                                                     select newWorkOrders;
            return query;

        }

        public static System.Linq.IQueryable<CWCayentaXchange.EEF.CINT_CAYENTA_STATUS> GetCityworksActualUpdates()
        {
            //get all work orders that have been processed to cayenta but have their actuals update flag as true
            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.CINT_CAYENTA_STATUS> query = from newWorkOrders in cwEntities.CINT_CAYENTA_STATUSs
                                                                                     where newWorkOrders.PROCESSEDTOCAYENTA == true //DEE - & have been sucessfully processed to cayenta
                                                                                     && newWorkOrders.UPDATEACTUALTOCAY == true
                                                                                     && newWorkOrders.CWSTATUS != "CLOSED"
                                                                                     select newWorkOrders;
            return query;

        }

        public static System.Linq.IQueryable<CWCayentaXchange.EEF.WORKORDERENTITY> GetCityworksAssets(string workorderid)
        {
            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.WORKORDERENTITY> query = from assets in cwEntities.WORKORDERENTITies
                                                                                 where assets.WORKORDERID == workorderid 
                                                                                 && assets.FEATURE_ID != 0 
                                                                                 && assets.ENTITYUID != "0"
                                                                                 select assets;
            return query;
               
        }

        public static System.Linq.IQueryable<CWCayentaXchange.EEF.LABORCOSTPRJ> GetEstimatedLabor(string workorderid)
        {
            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.LABORCOSTPRJ> query = from estlab in cwEntities.LABORCOSTPRJs
                                                                              where estlab.WORKORDERID == workorderid
                                                                              select estlab;
            return query;
        }

        public static System.Linq.IQueryable<CWCayentaXchange.EEF.LABORCOSTACT> GetActualLabor(string workorderid)
        {
            //DEE - Get Actual Labor By work order Id where cityworks transaction date is today....
            // and Description is '', 'Error', or 'Dirty xxxxx'

            DateTime dt = DateTime.Now;

            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.LABORCOSTACT> query = from actlab in cwEntities.LABORCOSTACTs
                                                                              where actlab.WORKORDERID == workorderid 
                                                                                 && actlab.TRANSDATE.Value.Year == dt.Year
                                                                                 && actlab.TRANSDATE.Value.Month == dt.Month
                                                                                 && actlab.TRANSDATE.Value.Day == dt.Day
                                                                                 && (actlab.DESCRIPTION == "" //Process New Ones
                                                                                 || actlab.DESCRIPTION == "Error" //Try and reprocess errors
                                                                                 || actlab.DESCRIPTION.StartsWith("Dirty") //Process Dirty Records
                                                                                 || actlab.DESCRIPTION.StartsWith("Pending")) //Grab Pending Deletion and Delete them after getting the crew card id 
                                                                              select actlab; //Yes we want the "Pending Delete From Crew Card" they could be the only ones where we normally rely on dirty
            return query;
        }

        public static System.Linq.IQueryable<CWCayentaXchange.EEF.LABORCOSTACT> GetActualLaborDates(string workorderid)
        {
            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.LABORCOSTACT> query = from estlab in cwEntities.LABORCOSTACTs
                                                                              where estlab.WORKORDERID == workorderid
                                                                              select estlab;
            return query;
        }

        public static System.Linq.IQueryable<CWCayentaXchange.EEF.MATERIALCOSTPRJ> GetEstimatedMaterial(string workorderid)
        {
            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.MATERIALCOSTPRJ> query = from estmat in cwEntities.MATERIALCOSTPRJs
                                                                                 where estmat.WORKORDERID == workorderid
                                                                                 select estmat;
            return query;
        }

        public static System.Linq.IQueryable<CWCayentaXchange.EEF.MATERIALCOSTACT> GetActualMaterial(string workorderid)
        {
            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.MATERIALCOSTACT> query = from estmat in cwEntities.MATERIALCOSTACTs
                                                                                 where estmat.WORKORDERID == workorderid
                                                                                 select estmat;
            return query;
        }

        public static System.Linq.IQueryable<CWCayentaXchange.EEF.EQUIPMENTCOSTPRJ> GetEstimatedEquipment(string workorderid)
        {
            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.EQUIPMENTCOSTPRJ> query = from esteq in cwEntities.EQUIPMENTCOSTPRJs
                                                                                 where esteq.WORKORDERID == workorderid
                                                                                 select esteq;
            return query;
        }

        public static System.Linq.IQueryable<CWCayentaXchange.EEF.EQUIPMENTCOSTACT> GetActualEquipment(string workorderid)
        {
            //DEE - Get Actual Equipment By work order Id where cityworks transaction date is today....

            DateTime dt = DateTime.Now;

            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.EQUIPMENTCOSTACT> query = from esteq in cwEntities.EQUIPMENTCOSTACTs
                                                                                  where esteq.WORKORDERID == workorderid
                                                                                  && esteq.TRANSDATE.Value.Year == dt.Year
                                                                                  && esteq.TRANSDATE.Value.Month == dt.Month
                                                                                  && esteq.TRANSDATE.Value.Day == dt.Day
                                                                                  && (esteq.ACCTNUM == "" //Process New Ones
                                                                                  || esteq.ACCTNUM == "Error" //Try and reprocess errors
                                                                                  || esteq.ACCTNUM.StartsWith("Dirty") //Process Dirty Records
                                                                                  || esteq.ACCTNUM.StartsWith("Pending")) //Grab Pending Deletion and Delete them after getting the crew card id 
                                                                                  select esteq;



            return query;
        }

        public static System.Linq.IQueryable<CWCayentaXchange.EEF.CINV_WORKORDER_PROJECT_STATUS> GetJobCostDetails(string workorderid)
        {
            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.CINV_WORKORDER_PROJECT_STATUS> query = from esteq in cwEntities.CINV_WORKORDER_PROJECT_STATUSs
                                                                                       where esteq.cay_WO_NUMBER == workorderid
                                                                                       select esteq;
            return query;
        }

        public static decimal GetContractorFromContractorID(string contractorid)
        {
            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.CONTRACTORLEAF> query = from ctleaf in cwEntities.CONTRACTORLEAves
                                                                                where ctleaf.CONTRACTORNUMBER == contractorid
                                                                                select ctleaf;
            decimal ctsid = 0;
            foreach (CWCayentaXchange.EEF.CONTRACTORLEAF c in query)
            {
                ctsid = c.CONTRACTORSID;
            }
            return ctsid;

        }
        public static string GetContractorNameFromContractorID(string contractorid)
        {
            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.CONTRACTORLEAF> query = from ctleaf in cwEntities.CONTRACTORLEAves
                                                                                where ctleaf.CONTRACTORNUMBER == contractorid
                                                                                select ctleaf;
            string ctname = string.Empty;
            foreach (CWCayentaXchange.EEF.CONTRACTORLEAF c in query)
            {
                ctname = c.CONTRACTORNAME;
            }
            return ctname;
        }

        public static decimal GetEquipmentFromEquipmentID(string equpimentid)
        {
            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.EQUIPMENTLEAF> query = from eqleaf in cwEntities.EQUIPMENTLEAves
                                                                               where eqleaf.EQUIPMENTUID == equpimentid
                                                                               select eqleaf;
            decimal eqsid = 0;
            foreach (CWCayentaXchange.EEF.EQUIPMENTLEAF e in query)
            {
                eqsid = e.EQUIPMENTSID;
            }
            return eqsid;
        }

        public static decimal GetMaterialFromMaterialID(string materialid)
        {
            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.MATERIALLEAF> query = from matleaf in cwEntities.MATERIALLEAves
                                                                              where matleaf.MATERIALUID == materialid
                                                                              select matleaf;
            decimal matsid = 0;
            foreach (CWCayentaXchange.EEF.MATERIALLEAF m in query)
            {
                matsid = m.MATERIALSID;
            }
            return matsid;

        }


        public static System.Linq.IQueryable<CWCayentaXchange.EEF.EMPLOYEE> GetEmployeeInfo(string employeeid)
        {
            //get employee sid from employee table and employee id

            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.EMPLOYEE> query = from emp in cwEntities.EMPLOYEEs
                                                                          where emp.EMPLOYEEID == employeeid
                                                                          select emp;

            return query;
        }

        public static string GetLabourResourceID(decimal laboursid) //changed to string 
        {
            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.EMPLOYEE> query = from emp in cwEntities.EMPLOYEEs
                                                                          where emp.EMPLOYEESID == laboursid
                                                                          select emp;

            string empid = string.Empty;
            foreach (CWCayentaXchange.EEF.EMPLOYEE e in query)
            {
                empid = e.EMPLOYEEID;
            }
            return empid;

        }

        public static EEF.EMPLOYEE GetEmployeeByEmployeeId(string employeeId)
        {
            EEF.Cityworks_PUD_Entities entities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.EMPLOYEE> query = from employee in entities.EMPLOYEEs
                                                                          where employee.EMPLOYEEID == employeeId
                                                                          select employee;
            if (query.Count() == 1)
            {
                return query.First();
            }
            else
            {
                return null;
            }
        }

        public static CWCayentaXchange.EEF.CONTRACTORLEAF GetContractorLeafByContractorNumber(string contractorNumber)
        {
            EEF.Cityworks_PUD_Entities entities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.CONTRACTORLEAF> query = from contractorLeaf in entities.CONTRACTORLEAves
                                                                                where contractorLeaf.CONTRACTORNUMBER == contractorNumber
                                                                                select contractorLeaf;
            if (query.Count() == 1)
            {
                return query.First();
            }
            else
            {
                return null;
            }
        }

        public static CWCayentaXchange.EEF.EQUIPMENTLEAF GetEquipmentLeafByUId(string equipmentUID)
        {
            EEF.Cityworks_PUD_Entities entities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.EQUIPMENTLEAF> query = from equipmentLeaf in entities.EQUIPMENTLEAves
                                                                               where equipmentLeaf.EQUIPMENTUID == equipmentUID
                                                                               select equipmentLeaf;
            if (query.Count() == 1)
            {
                return query.First();
            }
            else
            {
                return null;
            }
        }

        public static CWCayentaXchange.EEF.MATERIALLEAF GetMaterialLeafByUId(string materialUID)
        {
            EEF.Cityworks_PUD_Entities entities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.MATERIALLEAF> query = from materialleaf in entities.MATERIALLEAves
                                                                              where materialleaf.MATERIALUID == materialUID
                                                                              select materialleaf;
            if (query.Count() == 1)
            {
                return query.First();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Updates Cityworks Employee record
        /// </summary>
        /// <param name="cwEntities">Access to the Cityworks records</param>
        /// <param name="item">The incoming Cayenta record</param>
        /// <returns>True = it worked, False = it didn't work</returns>
        public static bool UpdateCityworksEmployee(EEF.Cityworks_PUD_Entities cwEntities, EEF.CAY_CW_EMPLOYEE item)
        {
            //This object is used to access and create the corresponding Cityworks data
            EEF.Cityworks_PUD_Entities entities = new EEF.Cityworks_PUD_Entities();
            string employeeId = item.employee_no.Trim();
            IQueryable<EEF.EMPLOYEE> employee = entities.EMPLOYEEs.Where(n => n.EMPLOYEEID == employeeId);
            if (employee == null || employee.Count() == 0) { return false; }

            Console.WriteLine("ISACTIVE: " + employee.First().ISACTIVE);
            if (!string.IsNullOrEmpty(item.last_nm)) 
            { 
                employee.First().LASTNAME = item.last_nm.Trim().ToUpper(); 
            }
            if (!string.IsNullOrEmpty(item.first_nm)) 
            { 
                employee.First().FIRSTNAME = item.first_nm.Trim().ToUpper(); 
            }
            if (!string.IsNullOrEmpty(item.email_address))
            { 
                employee.First().EMAIL = item.email_address.Trim().ToUpper(); 
            }
            if (!string.IsNullOrEmpty(item.description))
            {
                if (item.description.Trim() == "ACTIVE") { employee.First().ISACTIVE = "Y"; }
                else { employee.First().ISACTIVE = "N"; }
                Console.WriteLine("YEP");
            }
            else { employee.First().ISACTIVE = "N"; } 

            if (item.hourly_rate != null)
            {
                employee.First().HOURLYRATE = item.hourly_rate;
            }
            else { employee.First().HOURLYRATE = 0; }

            cwEntities.SaveChanges();
            
            Console.WriteLine("Updated Employee - Employee Number: " + employee.First().EMPLOYEEID);
            return true;
        }

        /// <summary>
        /// Add Cityworks Employee and create Service Request
        /// </summary>
        /// <param name="cwEntities">Access to the Cityworks records</param>
        /// <param name="item">The incoming Cayenta record</param>
        /// <returns>True = it worked, False = it didn't work</returns>
        public static bool AddCityworksEmployee(EEF.Cityworks_PUD_Entities cwEntities, EEF.CAY_CW_EMPLOYEE item)
        {
            //Get ID value (Employee System ID)  select * from azteca.PWSYSID where SYSTYPE = 'MISC'
            //Need to increment this value also.
            decimal employeeSId = getPwSysId("MISC");
            if (employeeSId == -1) { return false; }
            EEF.EMPLOYEE cwEmployee = EEF.EMPLOYEE.CreateEMPLOYEE(employeeSId, "LastName");
            cwEmployee.DOMAINID = 1;
            cwEmployee.EMPLOYEEID = item.employee_no.Trim();
            if (!string.IsNullOrEmpty(item.last_nm)) { cwEmployee.LASTNAME = item.last_nm.Trim().ToUpper(); }
            if (!string.IsNullOrEmpty(item.first_nm)) { cwEmployee.FIRSTNAME = item.first_nm.Trim().ToUpper(); }
            if (!string.IsNullOrEmpty(item.email_address)) { cwEmployee.EMAIL = item.email_address.Trim().ToUpper(); }
            if (!string.IsNullOrEmpty(item.description))
            {
                if (item.description.Trim() == "ACTIVE") { cwEmployee.ISACTIVE = "A"; }
                else { cwEmployee.ISACTIVE = "B"; }
            }
            else { cwEmployee.ISACTIVE = "C"; }
            if (item.hourly_rate != null)
            {
                cwEmployee.HOURLYRATE = item.hourly_rate;
            }
            else { cwEmployee.HOURLYRATE = 0; }

            cwEntities.EMPLOYEEs.AddObject(cwEmployee);

            Console.WriteLine("New Employee Added - EmployeesID: " + cwEmployee.EMPLOYEEID);

            return true;

        }

        private static bool addNewEmployeeServiceRequest(EEF.CAY_CW_EMPLOYEE item)
        {
            Uri cwUri = new Uri(ConfigurationManager.AppSettings["serviceUri"]);
            Cityworks.Core.Services factory = new Cityworks.Core.Services(cwUri);
            factory.Authenticate(ConfigurationManager.AppSettings["CWAPIUserID"], ConfigurationManager.AppSettings["CWAPIUserPwd"]);
            Cityworks.Core.ServiceRequestService.Create serviceRequest = new Cityworks.Core.ServiceRequestService.Create();
            EEF.PROBLEMLEAF problemLeaf = getProblemLeafFromCode(ConfigurationManager.AppSettings["ServiceRequestNewEmployeeCode"]);
            serviceRequest.ProblemSid = Convert.ToInt32(problemLeaf.PROBLEMSID);
            serviceRequest.Details = "ID: " + item.person_no;
            serviceRequest.Details += "\r\nFirst Name: " + item.first_nm.Trim();
            serviceRequest.Details += "\r\nLast Name: " + item.last_nm.Trim();
            serviceRequest.Details += "\r\nStatus: " + item.description.Trim();
            if (!string.IsNullOrEmpty(item.email_address)) { serviceRequest.Details += "\r\nEmail: " + item.email_address.Trim(); }

            serviceRequest.CallerFirstName = "Cayenta";
            serviceRequest.CallerLastName = "Cayenta";

            Cityworks.Core.ServiceRequestService.CreateResponse createServiceRequest = new Cityworks.Core.ServiceRequestService.CreateResponse();
            createServiceRequest = factory.AMS.ServiceRequest(serviceRequest);

            Console.WriteLine(createServiceRequest.Message);
            Console.WriteLine(createServiceRequest.Status);
            Console.WriteLine(createServiceRequest.Value);

            return true;
        }

        private static EEF.PROBLEMLEAF getProblemLeafFromCode(string code)
        {
            EEF.Cityworks_PUD_Entities entities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.PROBLEMLEAF> query = from problemleaf in entities.PROBLEMLEAves
                                                                             where problemleaf.PROBLEMCODE == code
                                                                             select problemleaf;
            if (query.Count() == 1) { return query.First(); }
            else { return null; }
        }

        /// <summary>
        /// Updates Cityworks Contractor Lead record
        /// </summary>
        /// <param name="cwEntities">Access to the Cityworks records</param>
        /// <param name="item">The incoming Cayenta record</param>
        /// <returns>True = it worked, False = it didn't work</returns>
        public static bool UpdateCityworksContractor(EEF.Cityworks_PUD_Entities cwEntities, EEF.CAY_CW_CONTRACTOR item)
        {
            //This object is used to access and create the corresponding Cityworks data
            EEF.Cityworks_PUD_Entities entities = new EEF.Cityworks_PUD_Entities();

            IQueryable<EEF.CONTRACTORLEAF> contractorLeaf = entities.CONTRACTORLEAves.Where(n => n.CONTRACTORNUMBER == item.contractor_no.Trim());
            if (contractorLeaf == null) { return false; }

             contractorLeaf.First().DESCRIPTION = item.name;
             contractorLeaf.First().CONTRACTORNAME = item.name;
             if (!string.IsNullOrEmpty(item.contact)) { contractorLeaf.First().CONTACTNAME = item.contact.Trim(); }
             if (!string.IsNullOrEmpty(item.address1)) { contractorLeaf.First().ADDRESS = item.address1.Trim(); }
             if (!string.IsNullOrEmpty(item.address2)) { contractorLeaf.First().ADDRESS += " " + item.address2.Trim(); }
             if (!string.IsNullOrEmpty(item.address3)) { contractorLeaf.First().ADDRESS += " " + item.address3.Trim(); }
             if (!string.IsNullOrEmpty(item.state)) { contractorLeaf.First().STATE = item.state.Trim(); }
             if (!string.IsNullOrEmpty(item.zip)) { contractorLeaf.First().ZIP = item.zip.Trim(); }
             if (!string.IsNullOrEmpty(item.phone1)) { contractorLeaf.First().OFFICEPHONE = item.phone1.Trim(); }
             if (!string.IsNullOrEmpty(item.phone2)) { contractorLeaf.First().OTHERPHONE = item.phone2.Trim(); }
             if (!string.IsNullOrEmpty(item.fax)) { contractorLeaf.First().FAX = item.fax.Trim(); }
             if (!string.IsNullOrEmpty(item.email_address)) { contractorLeaf.First().EMAIL = item.email_address.Trim(); }
            Console.WriteLine("Updated Contractor - Contractor Number: " + contractorLeaf.First().CONTRACTORNUMBER);
            return true;
        }

        /// <summary>
        /// Add Cityworks ContractorLeaf and ContractorNode records
        /// </summary>
        /// <param name="cwEntities">Access to the Cityworks records</param>
        /// <param name="item">The incoming Cayenta record</param>
        /// <returns>True = it worked, False = it didn't work</returns>
        public static bool AddCityworksContractor(EEF.Cityworks_PUD_Entities cwEntities, EEF.CAY_CW_CONTRACTOR item)
        {
            if (item.contractor_no == null) { return false; }

            //Get ID value (NODE ID)  select * from azteca.PWSYSID where SYSTYPE = 'MISC'
            //Need to increment this value also.
            decimal newNodesId = getPwSysId("MISC");
            if (newNodesId == -1) { return false; }

            decimal roonNodesId = getContractorRootNodesId();
            if (roonNodesId == -1) { return false; }

            EEF.CONTRACTORNODE contractorNode = EEF.CONTRACTORNODE.CreateCONTRACTORNODE(newNodesId, roonNodesId, item.contractor_no.Trim(), "L");
            contractorNode.DOMAINID = 1;
            bool locallyBased = true;
            if (item.state.Trim() != "WA") { locallyBased = false; }
            
            EEF.CONTRACTORLEAF contractorLeaf = EEF.CONTRACTORLEAF.CreateCONTRACTORLEAF(newNodesId, item.name.Trim(), locallyBased, true, false);
            if (!string.IsNullOrEmpty(item.contractor_no)) { contractorLeaf.CONTRACTORNUMBER = item.contractor_no.Trim(); }
            contractorLeaf.VIEWABLE = true;
            contractorLeaf.DESCRIPTION = item.name;
            contractorLeaf.CONTRACTORNAME = item.name;
            
            //The following are required by Cityworks but not provided by the Cayenta View
            contractorLeaf.RATETYPE = "A";
            contractorLeaf.OVERHEADTYPE = "A";
            contractorLeaf.RATE = 0;
            contractorLeaf.OVERHEADRATE = 0;
            contractorLeaf.OVERTIMEFACTOR = 0;
            contractorLeaf.PROVIDERTYPE = "001";
            contractorLeaf.LICENSED = "N";
            contractorLeaf.LIABILITYINSAMOUNT = 0;
            contractorLeaf.WORKERSCOMPAMOUNT = 0;
            contractorLeaf.AUTOMOBILEINSAMOUNT = 0;
            contractorLeaf.GENERALLIABILITYAMOUNT = 0;


            //*****************************************************************************

            if (!string.IsNullOrEmpty(item.contact)) { contractorLeaf.CONTACTNAME = item.contact.Trim(); }
            if (!string.IsNullOrEmpty(item.address1)) { contractorLeaf.ADDRESS = item.address1.Trim(); }
            if (!string.IsNullOrEmpty(item.address2)) { contractorLeaf.ADDRESS += " " + item.address2.Trim(); }
            if (!string.IsNullOrEmpty(item.address3)) { contractorLeaf.ADDRESS += " " + item.address3.Trim(); }
            if (!string.IsNullOrEmpty(item.state)) { contractorLeaf.STATE = item.state.Trim(); }
            if (!string.IsNullOrEmpty(item.zip)) { contractorLeaf.ZIP = item.zip.Trim(); }
            if (!string.IsNullOrEmpty(item.phone1)) { contractorLeaf.OFFICEPHONE = item.phone1.Trim(); }
            if (!string.IsNullOrEmpty(item.phone2)) { contractorLeaf.OTHERPHONE = item.phone2.Trim(); }
            if (!string.IsNullOrEmpty(item.fax)) { contractorLeaf.FAX = item.fax.Trim(); }
            if (!string.IsNullOrEmpty(item.email_address)) { contractorLeaf.EMAIL = item.email_address.Trim(); }

            cwEntities.CONTRACTORNODEs.AddObject(contractorNode);
            cwEntities.CONTRACTORLEAves.AddObject(contractorLeaf);
            Console.WriteLine("New Contractor Added - NodesID: " + contractorNode.NODESID);

            return true;

        }

        private static string getEquipmentDescription(EEF.CAY_CW_EQUIPMENT item)
        {
            string descriptionOut = string.Empty;

            if (item.YEAR != null) { descriptionOut = item.YEAR.ToString(); }

            if (item.MAKE != null)
            {
                if (!string.IsNullOrEmpty(descriptionOut)) { descriptionOut += " " + item.MAKE.Trim(); }
                else { descriptionOut = item.MAKE.Trim(); }
            }
            
            if (item.MODEL != null)
            {
                if (!string.IsNullOrEmpty(descriptionOut)) { descriptionOut += " " + item.MODEL.Trim(); }
                else { descriptionOut = item.MODEL.Trim(); }
            }

            return descriptionOut;
        }

        /// <summary>
        /// Updates Cityworks Material Lead record
        /// </summary>
        /// <param name="cwEntities">Access to the Cityworks records</param>
        /// <param name="item">The incoming Cayenta record</param>
        /// <returns>True = it worked, False = it didn't work</returns>
        public static bool UpdateCityworksEquipment(EEF.Cityworks_PUD_Entities cwEntities, EEF.CAY_CW_EQUIPMENT item)
        {
            //This object is used to access and create the corresponding Cityworks data
            EEF.Cityworks_PUD_Entities entities = new EEF.Cityworks_PUD_Entities();

            IQueryable<EEF.EQUIPMENTLEAF> equipmentLeaf = entities.EQUIPMENTLEAves.Where(n => n.EQUIPMENTUID == item.EQUIPMENT_NO.Trim());
            if (equipmentLeaf == null) { return false; }
            if (item.UNIT_COST != null)
            {
                equipmentLeaf.First().UNITCOST = item.UNIT_COST;
            }
            equipmentLeaf.First().DESCRIPTION = getEquipmentDescription(item);
            if (item.MODEL != null)
            {
                equipmentLeaf.First().MODEL = item.MODEL.Trim();
            }
            if (item.MAKE != null)
            {
                equipmentLeaf.First().MANUFACTURER = item.MAKE.Trim();
            }
            Console.WriteLine("Updated Material Added - MATERIALUID: " + equipmentLeaf.First().EQUIPMENTUID);
            return true;
        }

        /// <summary>
        /// Add Cityworks EquipmentLeaf and EquipmentNode records
        /// </summary>
        /// <param name="cwEntities">Access to the Cityworks records</param>
        /// <param name="item">The incoming Cayenta record</param>
        /// <returns>True = it worked, False = it didn't work</returns>
        public static bool AddCityworksEquipment(EEF.Cityworks_PUD_Entities cwEntities, EEF.CAY_CW_EQUIPMENT item)
        {
            //Get ID value (NODE ID)  select * from azteca.PWSYSID where SYSTYPE = 'MISC'
            //Need to increment this value also.
            decimal newNodesId = getPwSysId("MISC");
            if (newNodesId == -1) { return false; }

            decimal roonNodesId = getEquipmentRootNodesId();
            if (roonNodesId == -1) { return false; }

            EEF.EQUIPMENTNODE equipmentNode = EEF.EQUIPMENTNODE.CreateEQUIPMENTNODE(newNodesId, roonNodesId, item.EQUIPMENT_NO.Trim(), "L");
            equipmentNode.DOMAINID = 1;

            EEF.EQUIPMENTLEAF equipmentLeaf = EEF.EQUIPMENTLEAF.CreateEQUIPMENTLEAF(newNodesId, item.EQUIPMENT_NO.Trim(), "A", true);
            equipmentLeaf.DESCRIPTION = getEquipmentDescription(item);
            equipmentLeaf.UNITCOST = item.UNIT_COST;
            equipmentLeaf.EQUIPMENTUID = item.EQUIPMENT_NO.Trim();
            if (!string.IsNullOrEmpty(item.MODEL)) { equipmentLeaf.MODEL = item.MODEL.Trim(); }
            if (!string.IsNullOrEmpty(item.MAKE)) { equipmentLeaf.MANUFACTURER = item.MAKE.Trim(); }
           
            equipmentLeaf.VIEWABLE = true;

            cwEntities.EQUIPMENTNODEs.AddObject(equipmentNode);
            cwEntities.EQUIPMENTLEAves.AddObject(equipmentLeaf);
            Console.WriteLine("New Equipment Added - NodesID: " + equipmentNode.NODESID);

            return true;

        }

        /// <summary>
        /// Updates Cityworks Material Lead record
        /// </summary>
        /// <param name="cwEntities">Access to the Cityworks records</param>
        /// <param name="item">The incoming Cayenta record</param>
        /// <returns>True = it worked, False = it didn't work</returns>
        public static bool UpdateCityworksMaterial(EEF.Cityworks_PUD_Entities cwEntities, EEF.CAY_CW_INVENTORY item)
        {
            //This object is used to access and create the corresponding Cityworks data
            EEF.Cityworks_PUD_Entities entities = new EEF.Cityworks_PUD_Entities();

            IQueryable<EEF.MATERIALLEAF> materialLeaf = entities.MATERIALLEAves.Where(n => n.MATERIALUID == item.ITEM_NO.Trim());
            if (materialLeaf == null) { return false; }
            if (!string.IsNullOrEmpty(item.DESCRIPTION)) 
            { 
                materialLeaf.First().DESCRIPTION = item.DESCRIPTION.Trim(); 
            }
            if (!string.IsNullOrEmpty(item.PART_NUMBER)) 
            { 
                materialLeaf.First().PARTNUMBER = item.PART_NUMBER.Trim(); 
            }
            if (item.UNIT_COST != null)
            {
                materialLeaf.First().UNITCOST = item.UNIT_COST;
            }
            //materialLeaf.First().UNITCOST = item.UNIT_COST;

            Console.WriteLine("Updated Material Added - MATERIALUID: " + materialLeaf.First().MATERIALUID);
            return true;
        }

        /// <summary>
        /// Add Cityworks MaterialLeaf and MaterialNode records
        /// </summary>
        /// <param name="cwEntities">Access to the Cityworks records</param>
        /// <param name="item">The incoming Cayenta record</param>
        /// <returns>True = it worked, False = it didn't work</returns>
        public static bool AddCityworksMaterial(EEF.Cityworks_PUD_Entities cwEntities, EEF.CAY_CW_INVENTORY item)
        {
            //Get ID value (NODE ID)  select * from azteca.PWSYSID where SYSTYPE = 'MISC'
            //Need to increment this value also.
            decimal newNodesId = getPwSysId("MISC");
            if (newNodesId == -1) { return false; }

            decimal roonNodesId = getMaterialRootNodesId();
            if (roonNodesId == -1) { return false; }

            EEF.MATERIALNODE materialNode = EEF.MATERIALNODE.CreateMATERIALNODE(newNodesId, roonNodesId, item.ITEM_NO.Trim(), "L");
            materialNode.DOMAINID = 1;

            EEF.MATERIALLEAF materialLeaf = EEF.MATERIALLEAF.CreateMATERIALLEAF(newNodesId, item.ITEM_NO.Trim(), true);
            if (!string.IsNullOrEmpty(item.DESCRIPTION)) { materialLeaf.DESCRIPTION = item.DESCRIPTION.Trim(); }
            //materialLeaf.UNITCOST = item.UNIT_COST;
            if (!string.IsNullOrEmpty(item.PART_NUMBER)) { materialLeaf.PARTNUMBER = item.PART_NUMBER.Trim(); }
            materialLeaf.MINQUANTITY = Convert.ToDecimal(ConfigurationManager.AppSettings["MaterialDefaultMinQuant"]); //MaterialDefaultMinQuant
            materialLeaf.COSTTYPE = ConfigurationManager.AppSettings["MaterialDefaultCostType"]; //MaterialDefaultCostType
            materialLeaf.VIEWABLE = true;

            
            cwEntities.MATERIALNODEs.AddObject(materialNode);
            cwEntities.MATERIALLEAves.AddObject(materialLeaf);
            Console.WriteLine("New Material Added - NodesID: " + materialNode.NODESID);

            return true;

        }

        private static decimal getMaterialRootNodesId()
        {
            try
            {
                EEF.Cityworks_PUD_Entities entities = new EEF.Cityworks_PUD_Entities();
                string nodeName = ConfigurationManager.AppSettings["MaterialRootNodeName"];
                System.Linq.IQueryable<CWCayentaXchange.EEF.MATERIALNODE> materialNode = entities.MATERIALNODEs.Where(n => n.NODENAME == nodeName  && 
                    n.PARENTSID == 0 && n.NODETYPE == "R");
                if (materialNode.Count() == 1) { return materialNode.First().NODESID; }
                else { return -1; }
              }
            catch (Exception ex)
            {
                Logger.log.Error(ex.Message);
                Logger.log.Error(ex.StackTrace);
                return -1;
            }

        }

        private static decimal getEquipmentRootNodesId()
        {
            try
            {
                EEF.Cityworks_PUD_Entities entities = new EEF.Cityworks_PUD_Entities();
                string nodeName = ConfigurationManager.AppSettings["EquipmentRootNodeName"];
                System.Linq.IQueryable<CWCayentaXchange.EEF.EQUIPMENTNODE> equipmentNode = entities.EQUIPMENTNODEs.Where(n => n.NODENAME == nodeName &&
                    n.PARENTSID == 0 && n.NODETYPE == "R");
                if (equipmentNode.Count() == 1) { return equipmentNode.First().NODESID; }
                else { return -1; }
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex.Message);
                Logger.log.Error(ex.StackTrace);
                return -1;
            }

        }

        private static decimal getContractorRootNodesId()
        {
            try
            {
                EEF.Cityworks_PUD_Entities entities = new EEF.Cityworks_PUD_Entities();
                string nodeName = ConfigurationManager.AppSettings["ContractorRootNodeName"];
                System.Linq.IQueryable<CWCayentaXchange.EEF.CONTRACTORNODE> contractorNode = entities.CONTRACTORNODEs.Where(n => 
                    n.NODENAME == nodeName &&
                    n.PARENTSID == 0 && n.NODETYPE == "R");
                if (contractorNode.Count() == 1) { return contractorNode.First().NODESID; }
                else { return -1; }
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex.Message);
                Logger.log.Error(ex.StackTrace);
                return -1;
            }

        }

        private static decimal getPwSysId(string systype)
        {
            try
            {
                EEF.Cityworks_PUD_Entities entities = new EEF.Cityworks_PUD_Entities();
                System.Linq.IQueryable<CWCayentaXchange.EEF.PWSYSID> query = from pwsysids in entities.PWSYSIDs
                                                                             where pwsysids.SYSTYPE == systype
                                                                             select pwsysids;

                CWCayentaXchange.EEF.PWSYSID pwsysid = query.First();


                decimal id = pwsysid.ID;

                pwsysid.ID++;
                entities.SaveChanges();

                return id;
           
            }
            catch(Exception ex)
            {
                Logger.log.Error(ex.Message);
                Logger.log.Error(ex.StackTrace);
                return -1;
            }
        }
    }
}
