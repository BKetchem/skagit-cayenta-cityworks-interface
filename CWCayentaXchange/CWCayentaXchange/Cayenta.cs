﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Net;
using System.IO;
using System.Xml.Linq;
using System.Web.Services;
using System.Xml;
using System.Globalization;

namespace CWCayentaXchange
{
    public static class Cayenta
    {
        private static Uri serviceUri { get; set; }
       
        private static EEF.cayentaEntities cayenta { get; set; }


        public static bool CayentaWOOpen(EEF.WORKORDER cwWO)
        {
            //WP_NUMBER && WO_Number
            decimal wonumber = Convert.ToDecimal(cwWO.WORKORDERID);

            //WP_Prefix && WO_Prefix
            string wocategory = cwWO.WOCATEGORY;

            //Putting it all together here:
            string postData =   "<Request>" +
                                    "<GetWorkOrderInformation>" +
                                        "<Params>" +
			                                "<PREFIX>" + wocategory + "</PREFIX>" +
			                                "<NUMBER>" + wonumber + "</NUMBER>" +
			                                "<LINE_NO></LINE_NO>" +
			                                "<ATTRIBUTE_INFO>N</ATTRIBUTE_INFO>" +
			                                "<WORK_REQUEST_INFO>N</WORK_REQUEST_INFO>" +
		                                "</Params>" +
                                    "</GetWorkOrderInformation>"+
                                "</Request>";
            
            XmlDocument xmlReply = new XmlDocument();
            xmlReply = CayentaPost(postData, "<GetWorkOrderInformation>", cwWO.WORKORDERID);

            Console.WriteLine("Test");
            //Console.ReadLine();
           
            
            XmlNodeList Reply = xmlReply.SelectNodes("Reply");
            XmlNodeList GetWorkOrderInformation = Reply[0].SelectNodes("GetWorkOrderInformation");
            XmlNodeList Params = GetWorkOrderInformation[0].SelectNodes("Params");
            string ORDER_STATUS = Params[0].SelectSingleNode("ORDER_STATUS").InnerText;


            Console.WriteLine(ORDER_STATUS);
            
            if (ORDER_STATUS != "CLOSE")
            {
                Console.WriteLine ("work order was not closed");
                //Console.ReadLine();
                return true; //return true the work order is open
            }
            else 
            {
                Console.WriteLine("work order was closed");
                //Console.ReadLine();
                return false; //the work order is closed
                
            }
             
        }


        //DEE- method to post to cayenta, pass it a damn string and it will pass you damn xml...
        public static XmlDocument CayentaPost (String streamData, String callName, String woId)
        {
            XmlDocument xmlReply = new XmlDocument();
            try{
                //Declare
                WebRequest webRequest = null;
                WebResponse webResponse = null;
                Stream dataStream = null;
                StreamReader streamReader = null;
                string xmlResponse = string.Empty;
                byte[] byteArray = System.Text.Encoding.ASCII.GetBytes(streamData);

                //Request Header
                webRequest = WebRequest.Create(new Uri(ConfigurationManager.AppSettings["CayentaAPIUri"]));
                webRequest.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["CayentaUserID"], ConfigurationManager.AppSettings["CayentaLoginPwd"]);
                webRequest.PreAuthenticate = true;
                webRequest.ContentType = "text/xml";
                webRequest.Method = "POST";
                webRequest.ContentLength = byteArray.Length;

                //Post
                dataStream = webRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                //Response
                webResponse = webRequest.GetResponse();
                dataStream = webResponse.GetResponseStream();
                streamReader = new StreamReader(dataStream);
                xmlResponse = streamReader.ReadToEnd();

                //Dispose
                streamReader.Close();
                dataStream.Close();
                webResponse.Close();

                //Convert
                xmlReply.LoadXml(xmlResponse);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine("\nThe second HttpWebRequest object has raised an Argument Exception as 'Connection' Property is set to 'Close'");
                Console.WriteLine("\n{0}", e.Message);
                CityworksFunc.LogError(callName, woId, e.Message, "", "");
            }
            catch (WebException e)
            {
                Console.WriteLine("WebException raised!");
                Console.WriteLine("\n{0}", e.Message);
                Console.WriteLine("\n{0}", e.Status);
                CityworksFunc.LogError(callName, woId, e.Message, "", "");
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception raised!");
                Console.WriteLine("Source :{0} ", e.Source);
                Console.WriteLine("Message :{0} ", e.Message);
                CityworksFunc.LogError(callName, woId, e.Message, "", "");
            }

            //Return
            return xmlReply;
        }

        //Creates Work Request In Cayenta
        //Results of Cayenta API Call as an XMLDocument
        public static XmlDocument AddWorkRequest(EEF.WORKORDER cwWO)
        {
            XmlDocument xmlReply = new XmlDocument();
   
            //Get Job Cost Details
            System.Linq.IQueryable<CWCayentaXchange.EEF.CINV_WORKORDER_PROJECT_STATUS> jobCostDetails = CityworksFunc.GetJobCostDetails(cwWO.WORKORDERID);

            //WP_NUMBER && WO_Number
            decimal wonumber = Convert.ToDecimal(cwWO.WORKORDERID);

            //WP_Prefix && WO_Prefix
            string wocategory = cwWO.WOCATEGORY;

            //Same field for Job Type & Area //Pulling from Cayenta
            string woJobTypeArea = jobCostDetails.First().cay_JOB_TYPE_AREA;

            //Builiding from Cayenta JobNumber-Task-Subtask
            string woJobNumber = jobCostDetails.First().cay_JOB_NO;

            //WP_REQUIRED_DT
            string requiredTM = string.Empty;
            requiredTM = DateTime.Now.ToString("HHmmss");

            //Set the WP_REQUIRED_DT node if necessary
            string WP_REQUIRED_DT = string.Empty;
            if (cwWO.PROJSTARTDATE != null)
            {
                //DEE - If projected time is earlier then today use today, else use projected date.
                DateTime projstartdate = (DateTime)cwWO.PROJSTARTDATE;
                if (projstartdate < DateTime.Now)
                {
                    WP_REQUIRED_DT = DateTime.Now.ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"));
                }
                else
                {
                    WP_REQUIRED_DT = projstartdate.ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"));
                }
            }
            else
            {
                WP_REQUIRED_DT = DateTime.Now.ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"));
            }

            //WP_FULL_DESC
            string wpDescription = string.Empty;
            if(cwWO.DESCRIPTION != null)
            {
                wpDescription = cwWO.DESCRIPTION;
            }

            //WP_DESCRIPTION
            string wpShortDescription = string.Empty;
            if (cwWO.DESCRIPTION != null)
            {
                if (cwWO.DESCRIPTION.Length > 20)
                {
                    wpShortDescription = cwWO.DESCRIPTION.Substring(0, 20);
                }
                else
                {
                    wpShortDescription = cwWO.DESCRIPTION;
                }
            }

            //CREATE_WO
            string createWorkOrder = string.Empty;
            createWorkOrder = "Y";
        
            //if (cayenta == null) { cayenta = new EEF.cayentaEntities(); }

            //Putting it all together here:
            string postData =   "<Request>" +
                                    "<AddWorkRequest>" +
                                        "<Params>" +
                                            "<WP_PREFIX>"       + wocategory +          "</WP_PREFIX>" +
                                            "<WP_NUMBER>"       + wonumber +            "</WP_NUMBER>" +
                                            "<WO_PREFIX>"       + wocategory +          "</WO_PREFIX>" +
                                            "<WO_NUMBER>"       + wonumber +            "</WO_NUMBER>" +
                                            "<WP_TYPE>"         + woJobTypeArea +       "</WP_TYPE>" +
                                            "<WP_JOB_AREA>"     + woJobTypeArea +       "</WP_JOB_AREA>" +
                                            "<WP_JOB_NO>"       + woJobNumber +         "</WP_JOB_NO>" +
                                            "<WP_REQUIRED_DT>"  + WP_REQUIRED_DT +      "</WP_REQUIRED_DT>" +
                                            "<WP_REQUIRED_TM>"  + requiredTM +          "</WP_REQUIRED_TM>" +
                                            "<WP_DESCRIPTION>"  + wpShortDescription +  "</WP_DESCRIPTION>" +
                                            "<WP_FULL_DESC>"    + wpDescription +       "</WP_FULL_DESC>" +
                                            "<TEMPLATE_USER_ID></TEMPLATE_USER_ID>" +
                                            "<TEMPLATE_NM></TEMPLATE_NM>" +
                                            "<CREATE_WO>"       + createWorkOrder +     "</CREATE_WO>" +
                                        "</Params>" +
                                    "</AddWorkRequest>" +
                                "</Request>";

            xmlReply = CayentaPost(postData, "<AddWorkRequest>", cwWO.WORKORDERID);
            return xmlReply;
        }


        public static void getCayentaAssets(EEF.WORKORDER cwWO)
        {

            Console.WriteLine("??");
            //Console.ReadLine();

            EEF.Cityworks_PUD_Entities cwEntitiesDelete = new EEF.Cityworks_PUD_Entities();

            //delete all records on this work order that are Error or never attemted to process
            System.Linq.IQueryable<CWCayentaXchange.EEF.LABORCOSTACT> query = from actlab in cwEntitiesDelete.LABORCOSTACTs
                                                                              where actlab.WORKORDERID == cwWO.WORKORDERID
                                                                                 && actlab.DESCRIPTION != ""
                                                                                 && actlab.DESCRIPTION != "Error"
                                                                              select actlab;

            System.Linq.IQueryable<CWCayentaXchange.EEF.EQUIPMENTCOSTACT> query2 = from acteq in cwEntitiesDelete.EQUIPMENTCOSTACTs
                                                                                   where acteq.WORKORDERID == cwWO.WORKORDERID
                                                                                       && acteq.DESCRIPTION != "Error"
                                                                                       && acteq.DESCRIPTION != ""
                                                                                   select acteq;


            System.Linq.IQueryable<CWCayentaXchange.EEF.MATERIALCOSTACT> query3 = from actmat in cwEntitiesDelete.MATERIALCOSTACTs
                                                                                    where actmat.WORKORDERID == cwWO.WORKORDERID
                                                                                    select actmat;

            
            foreach (CWCayentaXchange.EEF.LABORCOSTACT asst in query)
            {
                cwEntitiesDelete.LABORCOSTACTs.DeleteObject(asst);
            }

            foreach (CWCayentaXchange.EEF.EQUIPMENTCOSTACT asst in query2)
            {
                cwEntitiesDelete.EQUIPMENTCOSTACTs.DeleteObject(asst);
            }

            foreach (CWCayentaXchange.EEF.MATERIALCOSTACT asst in query3)
            {
                cwEntitiesDelete.MATERIALCOSTACTs.DeleteObject(asst);
            }

            cwEntitiesDelete.SaveChanges();
            Console.WriteLine("Now Check and see if deleted!");
            //Console.ReadLine();



            //get the status of the work order in cayenta
            //will use it later to close the work order
            bool cayentaWOOpen = CayentaWOOpen(cwWO);
            
            //WP_NUMBER && WO_Number
            decimal wonumber = Convert.ToDecimal(cwWO.WORKORDERID);

            //WP_Prefix && WO_Prefix
            string wocategory = cwWO.WOCATEGORY;

            //Putting it all together here:
            string postData =   "<Request>" +
                                    "<GetWorkOrderCosts>" +
                                        "<Params>" +
                                            "<PREFIX>" + wocategory + "</PREFIX>" +
                                            "<ORDER_NUMBER>" + wonumber + "</ORDER_NUMBER>" +
                                            "<ORDER_LINE_NO>1</ORDER_LINE_NO>" +
                                            "<FROM_DT></FROM_DT><TO_DT></TO_DT>" +
                                        "</Params>" +
                                    "</GetWorkOrderCosts>" +
                                "</Request>";

            XmlDocument xmlReply = new XmlDocument();
            xmlReply = CayentaPost(postData, "<AddWorkRequest>", cwWO.WORKORDERID); // cwWO.WORKORDERID);

            //parse xml reply & write to cityworks
            XmlNodeList Reply = xmlReply.SelectNodes("Reply");
            XmlNodeList GetWorkOrderCosts = Reply[0].SelectNodes("GetWorkOrderCosts");
            XmlNode Paramnode = xmlReply.SelectSingleNode("//Params");
            XmlNodeList transactions = Paramnode.SelectNodes("..//Transaction");

            for (int i = 0; i < transactions.Count; i++)
            {
                string tdate = transactions[i].SelectSingleNode("TRXN_DT").InnerText;
                string ttype = transactions[i].SelectSingleNode("RESOURCE_TYPE").InnerText;
                string tresourceid = transactions[i].SelectSingleNode("RESOURCE_ID").InnerText;
                string tquantity = transactions[i].SelectSingleNode("QUANTITY").InnerText;
                string trate = transactions[i].SelectSingleNode("RATE").InnerText;
                string tamount = transactions[i].SelectSingleNode("AMOUNT").InnerText;

                //Convert Date To Date Duhh... 
                DateTime transactionDate = DateTime.ParseExact(tdate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None);

                //write to labor cost table
                if (ttype == "PA")
                {
                    EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
                    EEF.LABORCOSTACT newLabor = new EEF.LABORCOSTACT();
                    IQueryable<EEF.EMPLOYEE> cwEmployee = cwEntities.EMPLOYEEs.Where(n => n.EMPLOYEEID == tresourceid);

                    Console.WriteLine("PA " + tresourceid + " " + cwEmployee.First().LASTNAME + ", " + cwEmployee.First().FIRSTNAME);

                    newLabor.WORKORDERID = cwWO.WORKORDERID; // cwWO.WORKORDERID;
                    newLabor.DOMAINID = 1; 
                    newLabor.LABORTYPE = "A";
                    newLabor.LABORSID = cwEmployee.First().EMPLOYEESID;
                    newLabor.LABORNAME = cwEmployee.First().LASTNAME + ", " + cwEmployee.First().FIRSTNAME;
                    newLabor.DESCRIPTION = "Cayenta Approved";
                    newLabor.RATETYPE = "A";
                    newLabor.HOURS = Convert.ToDecimal(tquantity);
                    newLabor.STARTDATE = transactionDate;  //convert
                    newLabor.FINISHDATE = transactionDate; //convert
                    newLabor.COST = Convert.ToDecimal(tamount);
                    newLabor.TRANSDATE = DateTime.Now;
                    newLabor.REGULARCOST = Convert.ToDecimal(tamount);

                    cwEntities.LABORCOSTACTs.AddObject(newLabor);
                    cwEntities.SaveChanges();
                }
                if (ttype == "AP")
                {
                    EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
                    EEF.LABORCOSTACT newLabor = new EEF.LABORCOSTACT();
                    IQueryable<EEF.CONTRACTORLEAF> cwContractor = cwEntities.CONTRACTORLEAves.Where(n => n.CONTRACTORNUMBER == tresourceid);

                    Console.WriteLine("AP " + tresourceid + " " + cwContractor.First().CONTRACTORNAME);

                    newLabor.WORKORDERID = cwWO.WORKORDERID; // cwWO.WORKORDERID;
                    newLabor.DOMAINID = 1;
                    newLabor.LABORTYPE = "B";
                    newLabor.LABORNAME = cwContractor.First().CONTRACTORNAME;
                    newLabor.CONTRACTORNUMBER = tresourceid;
                    newLabor.DESCRIPTION = "Cayenta Approved";
                    newLabor.RATETYPE = "A";
                    newLabor.HOURS = Convert.ToDecimal(tquantity);
                    newLabor.STARTDATE = transactionDate;  //convert
                    newLabor.FINISHDATE = transactionDate; //convert
                    newLabor.COST = Convert.ToDecimal(tamount);
                    newLabor.LABORSID = cwContractor.First().CONTRACTORSID;
                    newLabor.TRANSDATE = DateTime.Now;
                    newLabor.REGULARCOST = Convert.ToDecimal(tamount);

                    cwEntities.LABORCOSTACTs.AddObject(newLabor);
                    cwEntities.SaveChanges();
                }
                if (ttype == "EQ")
                {
                    EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
                    EEF.EQUIPMENTCOSTACT newEquip = new EEF.EQUIPMENTCOSTACT();
                    IQueryable<EEF.EQUIPMENTLEAF> cwEquipment = cwEntities.EQUIPMENTLEAves.Where(n => n.EQUIPMENTUID == tresourceid);

                    Console.WriteLine("EQ " + tresourceid + " " + cwEquipment.First().DESCRIPTION);

                    newEquip.WORKORDERID = cwWO.WORKORDERID; // cwWO.WORKORDERID;
                    newEquip.DESCRIPTION = cwEquipment.First().DESCRIPTION;
                    newEquip.HOURSREQUIRED = Convert.ToDecimal(tquantity);
                    newEquip.UNITSREQUIRED = 1;
                    newEquip.RATETYPE = "A";
                    newEquip.COST = Convert.ToDecimal(tamount);
                    newEquip.SOURCE = "Inventory";
                    newEquip.EQUIPMENTSID = cwEquipment.First().EQUIPMENTSID;
                    newEquip.EQUIPMENTUID = cwEquipment.First().EQUIPMENTUID;
                    newEquip.ACCTNUM = "Cayenta Approved";
                    newEquip.TRANSDATE = DateTime.Now;
                    newEquip.DOMAINID = 1;
                    newEquip.STARTDATE = transactionDate;  //convert
                    newEquip.FINISHDATE = transactionDate; //convert

                    cwEntities.EQUIPMENTCOSTACTs.AddObject(newEquip);
                    cwEntities.SaveChanges();
                }//if eq 
                if (ttype == "IN")
                {
                    EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
                    EEF.MATERIALCOSTACT newMat = new EEF.MATERIALCOSTACT();
                    IQueryable<EEF.MATERIALLEAF> cwMaterial = cwEntities.MATERIALLEAves.Where(n => n.MATERIALUID == tresourceid);

                    Console.WriteLine("IN " + tresourceid + " " + cwMaterial.First().DESCRIPTION);

                    newMat.WORKORDERID = cwWO.WORKORDERID; // cwWO.WORKORDERID;
                    newMat.DESCRIPTION = cwMaterial.First().DESCRIPTION;
                    newMat.UNITSREQUIRED = Convert.ToDecimal(tquantity);
                    newMat.COST = Convert.ToDecimal(tamount);
                    newMat.SOURCE = "N/A";
                    newMat.STOCKMODIFIED = "N";
                    newMat.MATERIALSID = cwMaterial.First().MATERIALSID;
                    newMat.MATERIALUID = cwMaterial.First().MATERIALUID;
                    newMat.ACCTNUM = "Cayenta Approved";
                    newMat.TRANSDATE = transactionDate; //Different from all the other ones, Material in CW doesnt have Start or Stop Date.
                    newMat.DOMAINID = 1;

                    cwEntities.MATERIALCOSTACTs.AddObject(newMat);
                    cwEntities.SaveChanges();
                } //if in
            } //foreach

                Console.WriteLine("Now Check and see if saved!");
                //Console.ReadLine();
                

            if (cayentaWOOpen == false) //if the cayenta wo is not open close out the work order this is the last time we will 
            {
                cwWO.STATUS = "CLOSED";
                cwWO.WOCLOSEDBY = "CAYENTA";
                cwWO.DATEWOCLOSED = DateTime.Now;
                //cwEntities.CINT_CAYENTA_STATUSs.ApplyCurrentValues(wo);
                //cwEntities.SaveChanges();
            }

            //mark the work order as closed if it was closed

        }

//CayentaServiceSucess
        public static bool CayentaServiceSucess(XmlDocument cayentaWO, String woId)
        {
            try //DEE - Adding more error handling to catch no connection issue. 
            {
                XmlNodeList Reply = cayentaWO.SelectNodes("Reply");
                XmlNodeList AddWorkRequest = Reply[0].SelectNodes("AddWorkRequest");
                XmlNodeList STATUS = AddWorkRequest[0].SelectNodes("STATUS"); // DEE - communication status
                XmlNodeList Params = AddWorkRequest[0].SelectNodes("Params");
                //XmlNodeList STATUS = Params[0].SelectNodes("STATUS"); //DEE - request status


                if (STATUS[0].InnerText == "0")
                {
                    return true; //DEE - communication status sucessfull
                }
                else
                {
                    CityworksFunc.LogError("CayentaServiceSucess", woId, STATUS[0].InnerText, "", "");
                    return false;
                }
            }
            catch
            {
                CityworksFunc.LogError("CayentaServiceSucess", woId, "ERROR CONNECTING TO CAYENTA API", "", "");
                return false;
            }
        }

//CayentaWOCreated
        public static bool CayentaWOCreated(XmlDocument cayentaWO, string woId)
        {
            XmlNodeList Reply = cayentaWO.SelectNodes("Reply");
            XmlNodeList AddWorkRequest = Reply[0].SelectNodes("AddWorkRequest");
            //XmlNodeList STATUS = AddWorkRequest[0].SelectNodes("STATUS"); // DEE - communication status
            XmlNodeList Params = AddWorkRequest[0].SelectNodes("Params");
            XmlNodeList STATUS = Params[0].SelectNodes("STATUS"); //DEE - request status
            XmlNodeList STATUS_DESC = Params[0].SelectNodes("STATUS_DESC"); //DEE - request status

            //Update Cityworks
            //CityworksFunc.UpdateCINT_CAYENTA_STATUSS(cayentaWO, woId);

            if (STATUS[0].InnerText == "0")
            {
                return true;

            }
            else
            {
                CityworksFunc.LogError("<AddWorkRequest>", woId, STATUS_DESC[0].InnerText, "", "");
                return false;
            }
            
        }

        public static bool CayentaWOUpdated(XmlDocument cayentaWO)
        {
            XmlNodeList Reply = cayentaWO.SelectNodes("Reply");
            XmlNodeList setWorkOrder = Reply[0].SelectNodes("SetWorkOrder");
            XmlNodeList parameters = setWorkOrder[0].SelectNodes("Params");
            XmlNodeList status = parameters[0].SelectNodes("STATUS");
            if (status[0].InnerText == "0")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool CayentaWRUpdated(XmlDocument cayentaWR)
        {
            XmlNodeList Reply = cayentaWR.SelectNodes("Reply");
            XmlNodeList setWorkOrder = Reply[0].SelectNodes("SetWorkRequest");
            XmlNodeList parameters = setWorkOrder[0].SelectNodes("Params");
            XmlNodeList status = parameters[0].SelectNodes("STATUS");
            if (status[0].InnerText == "0")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /* //DEE- Delete?
        public static XmlDocument AddWorkRequestAssetsLabor()
        {

            XmlDocument xmlReply = new XmlDocument();
            //xmlReply.LoadXml(xml);
            return xmlReply;
        }

        public static XmlDocument AddWorkRequestAssetsMaterial()
        {

            XmlDocument xmlReply = new XmlDocument();
            //xmlReply.LoadXml(xml);
            return xmlReply;
        }

        public static XmlDocument AddWorkRequestAssetsEquipment()
        {

            XmlDocument xmlReply = new XmlDocument();
            //xmlReply.LoadXml(xml);
            return xmlReply;
        }

        public static XmlDocument AddWorkRequestAssetsContractors()
        {

            XmlDocument xmlReply = new XmlDocument();
            //xmlReply.LoadXml(xml);
            return xmlReply;
        }

        public static void AddWorkRequestResources()
        {

        }
        */
         

        /*
         * The AddWorkRequestAssets function allows the external system to add assets to a Work Request line.  
         * Message identifier is <AddWorkRequestAssets>.  This function adds or updates assets.  If function is called a subsequent time with the same asset, 
         * the asset is not added again.  
         */
        public static XmlDocument AddWorkRequestAsset(EEF.WORKORDER cwWO)
        {
            XmlDocument xmlReply = new XmlDocument();

            //Number
            decimal wonumber = Convert.ToDecimal(cwWO.WORKORDERID);

            //Prefix
            string wocategory = cwWO.WOCATEGORY;

            string assetList = string.Empty;

            System.Linq.IQueryable<CWCayentaXchange.EEF.WORKORDERENTITY> workorderassets = CityworksFunc.GetCityworksAssets(cwWO.WORKORDERID);
            
            foreach (CWCayentaXchange.EEF.WORKORDERENTITY asst in workorderassets)
            {
                string assetArea = string.Empty;
                if (wocategory.Length > 1)
                {
                    assetArea = wocategory.Substring(0, 2);
                }
                string assetType = string.Empty;
                if(asst.ENTITYUID.Length > 1)
                {
                    assetType = asst.ENTITYUID.Substring(0, 2);
                }

                assetList = assetList + 
                            "<ASSET>" +
                            "<ASSET_AREA>" + assetArea + "</ASSET_AREA>" +
                            "<ASSET_TYPE>" + assetType + "</ASSET_TYPE>" +
                            "<ASSET_NO>" + asst.FEATURE_ID + "</ASSET_NO>" +
                            "</ASSET>";
                                
            }

            
            //Putting it all together here:
            string postData = "<Request>" +
                                "<AddWorkRequestAssets>" +
                                    "<Params>" +
                                        "<PREFIX>" + wocategory + "</PREFIX>" +
                                        "<NUMBER>" + wonumber + "</NUMBER>" +
                                        "<LINE_NO>1</LINE_NO>" +
                                        "<ASSETS>" 
                                        + assetList +
                                        "</ASSETS>" +
                                "</Params>" +
                            "</AddWorkRequestAssets>" +
                          "</Request>";

            xmlReply = CayentaPost(postData, "<AddWorkRequestAssets>", cwWO.WORKORDERID);
            return xmlReply;

        }

        public static XmlDocument AddWorkRequestEstimatedResources(EEF.WORKORDER cwWO)
        {
            XmlDocument xmlReply = new XmlDocument();

            //Number
            decimal wonumber = Convert.ToDecimal(cwWO.WORKORDERID);

            //Prefix
            string wocategory = cwWO.WOCATEGORY;

            string resourceList = string.Empty;

            //get estimated labor
            System.Linq.IQueryable<CWCayentaXchange.EEF.LABORCOSTPRJ> laborcostest = CityworksFunc.GetEstimatedLabor(cwWO.WORKORDERID);
            foreach (CWCayentaXchange.EEF.LABORCOSTPRJ asst in laborcostest)
            {
                //DEE = translate laborsid on estimated table to resource id from employee table
                string resourceID = CityworksFunc.GetLabourResourceID(Convert.ToDecimal(asst.LABORSID));
                //DEE - build call
                resourceList = resourceList + 
                                "<RESOURCE><RESOURCE_TYPE>LA</RESOURCE_TYPE>" +
                                "<RESOURCE_ID>" + resourceID  + "</RESOURCE_ID>" +
                                "<QUANTITY>" + asst.HOURS + "</QUANTITY>" + 
                                "</RESOURCE>";
            }

            //get estimated materials
            System.Linq.IQueryable<CWCayentaXchange.EEF.MATERIALCOSTPRJ> materialcostest = CityworksFunc.GetEstimatedMaterial(cwWO.WORKORDERID);
            foreach (CWCayentaXchange.EEF.MATERIALCOSTPRJ asst in materialcostest)
            {
                resourceList = resourceList +
                                "<RESOURCE><RESOURCE_TYPE>IN</RESOURCE_TYPE>" +
                                "<RESOURCE_ID>" + asst.MATERIALUID + "</RESOURCE_ID>" +
                                "<QUANTITY>" + asst.UNITSREQUIRED + "</QUANTITY>" +
                                "</RESOURCE>";
            }
            //get estimated equipment
            System.Linq.IQueryable<CWCayentaXchange.EEF.EQUIPMENTCOSTPRJ> equipmentcostest = CityworksFunc.GetEstimatedEquipment(cwWO.WORKORDERID);
            foreach (CWCayentaXchange.EEF.EQUIPMENTCOSTPRJ asst in equipmentcostest)
            {
                resourceList = resourceList +
                               "<RESOURCE><RESOURCE_TYPE>EQ</RESOURCE_TYPE>" +
                               "<RESOURCE_ID>" + asst.EQUIPMENTUID + "</RESOURCE_ID>" +
                               "<QUANTITY>" + asst.HOURSREQUIRED + "</QUANTITY>" +
                               "</RESOURCE>";
            }

            //Putting it all together here:
            if (resourceList == "")
            {
                return null;
            }
            string postData = "<Request>" +
                                "<AddWorkRequestResources>" +
                                    "<Params>" +
                                        "<PREFIX>" + wocategory + "</PREFIX>" +
                                        "<NUMBER>" + wonumber + "</NUMBER>" +
                                        "<LINE_NO>1</LINE_NO>" +
                                        "<RESOURCES>"
                                        + resourceList +
                                        "</RESOURCES>" +
                                "</Params>" +
                            "</AddWorkRequestResources>" +
                          "</Request>";

            xmlReply = CayentaPost(postData, "<AddWorkRequestResources>", cwWO.WORKORDERID);
            return xmlReply;

        }

        //each crew card is by day so we process it it a subsert of get actuals hence the new query
        public static bool UpdateCityworksActualResources(EEF.WORKORDER cwWO, int v_sucess, DateTime v_date, string v_specific)
        {
            //Get labor and equipment with Cityworks Transaction Date of Today
            //IQueryable<EEF.LABORCOSTACT> laborcostact = CityworksFunc.GetActualLabor(cwWO.WORKORDERID);
            //IQueryable<EEF.EQUIPMENTCOSTACT> equipmentcostact = CityworksFunc.GetActualEquipment(cwWO.WORKORDERID);

            String cwDescription = null;
            if (v_sucess == 0)
            {
                cwDescription = "Sent To Crew Card " + v_specific;
            }
            else
            {
                cwDescription = "Error"; // +v_specifcError;
            }

            DateTime dt = DateTime.Now;
            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            System.Linq.IQueryable<CWCayentaXchange.EEF.LABORCOSTACT> query = from actlab in cwEntities.LABORCOSTACTs
                                                                              where actlab.WORKORDERID == cwWO.WORKORDERID
                                                                                 && actlab.TRANSDATE.Value.Year == dt.Year
                                                                                 && actlab.TRANSDATE.Value.Month == dt.Month
                                                                                 && actlab.TRANSDATE.Value.Day == dt.Day
                                                                                 && (actlab.DESCRIPTION == ""
                                                                                 || actlab.DESCRIPTION == "Error"
                                                                                 || actlab.DESCRIPTION.StartsWith("Dirty"))
                                                                                 && actlab.STARTDATE.Value.Year == v_date.Year
                                                                                 && actlab.STARTDATE.Value.Month == v_date.Month
                                                                                 && actlab.STARTDATE.Value.Day == v_date.Day
                                                                              select actlab;


            System.Linq.IQueryable<CWCayentaXchange.EEF.EQUIPMENTCOSTACT> query2 = from acteq in cwEntities.EQUIPMENTCOSTACTs
                                                                                      where acteq.WORKORDERID == cwWO.WORKORDERID
                                                                                      && acteq.TRANSDATE.Value.Year == dt.Year
                                                                                      && acteq.TRANSDATE.Value.Month == dt.Month
                                                                                      && acteq.TRANSDATE.Value.Day == dt.Day
                                                                                      && (acteq.ACCTNUM == ""
                                                                                      || acteq.ACCTNUM == "Error"
                                                                                      || acteq.ACCTNUM.StartsWith("Dirty"))
                                                                                      && acteq.STARTDATE.Value.Year == v_date.Year
                                                                                      && acteq.STARTDATE.Value.Month == v_date.Month
                                                                                      && acteq.STARTDATE.Value.Day == v_date.Day
                                                                                   select acteq;

            foreach (CWCayentaXchange.EEF.LABORCOSTACT actlab in query)
            {
                actlab.DESCRIPTION = cwDescription; 
                Console.WriteLine("Write Actual Labor");
                //Console.ReadLine();
            }

            foreach (CWCayentaXchange.EEF.EQUIPMENTCOSTACT acteq in query2)
            {
                acteq.ACCTNUM = cwDescription;
                Console.WriteLine("Write Actual Equipment ");
                //Console.ReadLine();
            }

            cwEntities.SaveChanges();

            return true;
        }

        //DEE - Need to delete the dirty crew card from cayenta, and delete the Pending Delete Transactions from Cityworks 
        public static bool DeleteCrewCards(EEF.WORKORDER cwWO) 
        {

            
            EEF.Cityworks_PUD_Entities cwEntities = new EEF.Cityworks_PUD_Entities();
            DateTime dt = DateTime.Now;
            System.Linq.IQueryable<CWCayentaXchange.EEF.LABORCOSTACT> query = from actlab in cwEntities.LABORCOSTACTs
                                                                              where actlab.WORKORDERID == cwWO.WORKORDERID
                                                                                 && actlab.TRANSDATE.Value.Year == dt.Year
                                                                                 && actlab.TRANSDATE.Value.Month == dt.Month
                                                                                 && actlab.TRANSDATE.Value.Day == dt.Day
                                                                                 && (actlab.DESCRIPTION.StartsWith("Dirty") //Process Dirty Records
                                                                                 || actlab.DESCRIPTION.StartsWith("Pending")) //Grab Pending Deletion and Delete them after getting the crew card id 
                                                                              select actlab;

            System.Linq.IQueryable<CWCayentaXchange.EEF.EQUIPMENTCOSTACT> query2 = from acteq in cwEntities.EQUIPMENTCOSTACTs
                                                                                   where acteq.WORKORDERID == cwWO.WORKORDERID
                                                                                   && acteq.TRANSDATE.Value.Year == dt.Year
                                                                                   && acteq.TRANSDATE.Value.Month == dt.Month
                                                                                   && acteq.TRANSDATE.Value.Day == dt.Day
                                                                                   && (acteq.ACCTNUM.StartsWith("Dirty")
                                                                                   || acteq.ACCTNUM.StartsWith("Pending"))
                                                                                   select acteq;


            var crewCards = new List<String>();

            //get all the crew cards dirty crew cards from labor
            foreach (CWCayentaXchange.EEF.LABORCOSTACT asst in query)
            {
                
                string s = asst.DESCRIPTION;

                //DEE - Put the crew card it in to a list for later deleteion 
                if (s.ToLowerInvariant().IndexOf('(') != -1)
                {
                    int start = s.IndexOf("(") + 1;
                    int end = s.IndexOf(")", start);
                    string result = s.Substring(start, end - start);
                    Console.WriteLine(result);
                    crewCards.Add(result);
                }
                else
                {
                    Console.WriteLine(s);
                }
                    //Console.ReadLine();

                //DEE Get all the cityworks records that need to be deleted and delete them. 
                if(asst.DESCRIPTION.StartsWith("Pending"))
                {
                    //delete
                    Console.WriteLine(asst.DESCRIPTION);
                    //Console.ReadLine();
                    //cwEntities.LABORCOSTACTs.Attach(asst);
                    cwEntities.LABORCOSTACTs.DeleteObject(asst);
                    //cwEntities.SaveChanges(); moved outside the read
                    Console.WriteLine("Deleted?");
                    //Console.ReadLine();
                }


            }

            foreach (CWCayentaXchange.EEF.EQUIPMENTCOSTACT asst in query2)
            {

                string s = asst.ACCTNUM; //equipment we are storing the crew card status in account

                //DEE - Put the crew card it in to a list for later deleteion 
                if (s.ToLowerInvariant().IndexOf('(') != -1)
                {
                    int start = s.IndexOf("(") + 1;
                    int end = s.IndexOf(")", start);
                    string result = s.Substring(start, end - start);
                    Console.WriteLine(result);
                    crewCards.Add(result);
                }
                else
                {
                    Console.WriteLine(s);
                }
                //Console.ReadLine();

                //DEE Get all the cityworks records that need to be deleted and delete them. 
                if (asst.ACCTNUM.StartsWith("Pending"))
                {
                    //delete
                    Console.WriteLine(asst.ACCTNUM);
                    //Console.ReadLine();
                    //cwEntities.LABORCOSTACTs.Attach(asst);
                    cwEntities.EQUIPMENTCOSTACTs.DeleteObject(asst);
                    //cwEntities.SaveChanges(); moved outside the read
                    Console.WriteLine("Equipment Deleted?");
                    //Console.ReadLine();
                }


            }

            cwEntities.SaveChanges();
            Console.WriteLine("Now Check and see if deleted!");
            //Console.ReadLine();

            //Get distinct crew cards and delete them from cayenta
            var crewCardsDistinct = crewCards.Distinct();

            string wocategory = cwWO.WOCATEGORY;
            decimal wonumber = Convert.ToDecimal(cwWO.WORKORDERID);
            
            foreach (var card in crewCardsDistinct)
            {
                string batch = card.Substring(0, card.IndexOf('-'));
                string sheet = card.Substring(card.LastIndexOf('-') + 1);

                string postData =   "<Request>" +
	                                    "<DeleteWOCost>" +
		                                    "<Params>" +
			                                    "<PREFIX>" + wocategory + "</PREFIX>" +
			                                    "<ORDER_NUMBER>" + wonumber + "</ORDER_NUMBER>" +
			                                    "<ORDER_LINE_NO>" + 1 + "</ORDER_LINE_NO>" +
			                                    "<ORIGINAL_SS></ORIGINAL_SS>" +
			                                    "<BATCH_NO>" + batch + "</BATCH_NO>" +
			                                    "<SHEET_NO>" + sheet + "</SHEET_NO>" +
		                                    "</Params>"+
	                                    "</DeleteWOCost>"+
                                    "</Request>";

                XmlDocument xmlReply = new XmlDocument();
                xmlReply = CayentaPost(postData, "<DeleteWOCost>", cwWO.WORKORDERID);

                Console.WriteLine(postData);
                //Console.ReadLine();
            }

            return true;
        }

        public static bool AddWorkRequestActualResources(EEF.WORKORDER cwWO)
        {

            //Delete the Pending Delete before calling for them later becuase we use the same call
            //GetActualLabor
            DeleteCrewCards(cwWO);

            bool updatesucessful = true;
            
            //Number
            decimal wonumber = Convert.ToDecimal(cwWO.WORKORDERID);

            //Prefix
            string wocategory = cwWO.WOCATEGORY;

            string resourceList = string.Empty;

            XmlDocument xmlCostReply = new XmlDocument();
            xmlCostReply = null;

            //get estimated labor and equipment with a cityworks transaction date of today 
            System.Linq.IQueryable<CWCayentaXchange.EEF.LABORCOSTACT> laborcostact = CityworksFunc.GetActualLabor(cwWO.WORKORDERID);
            System.Linq.IQueryable<CWCayentaXchange.EEF.EQUIPMENTCOSTACT> equipmentcostact = CityworksFunc.GetActualEquipment(cwWO.WORKORDERID);

            //DEE need to create crew cards based on dates
            var dates = new List<DateTime>();


            if (laborcostact.Count() > 0 || equipmentcostact.Count() > 0)
            {
                //PSEUDO CODE
                //-Select All transactions not processed to cayenta 
                //DEE - create a distinct list of startdates
                foreach (CWCayentaXchange.EEF.LABORCOSTACT asst in laborcostact)
                {
                    DateTime cwStartDate = (DateTime)asst.STARTDATE;
                    dates.Add(cwStartDate);
                }

                foreach (CWCayentaXchange.EEF.EQUIPMENTCOSTACT asst in equipmentcostact)
                {
                    DateTime cwStartDate = (DateTime)asst.STARTDATE;
                    dates.Add(cwStartDate);
                }

                var datesDistinct = dates.Distinct();

                foreach (var date in datesDistinct)
                {
                    Console.WriteLine(date);


                    //actuals exist so create crew card
                    //get the transaction date example 20140530

                    string startDate = date.ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"));

                    Console.WriteLine(startDate);
                    //Console.ReadLine();

                    //Putting it all together here:
                    string postData = "<Request>" +
                                        "<AddWOCost>" +
                                            "<Params>" +
                                                "<PREFIX>" + wocategory + "</PREFIX>" +
                                                "<ORDER_NUMBER>" + wonumber + "</ORDER_NUMBER>" +
                                                "<ORDER_LINE_NO>1</ORDER_LINE_NO>" +
                                                "<TRANS_DATE>" + startDate + "</TRANS_DATE>" +
                                                "<ENTRY_SOURCE>V5</ENTRY_SOURCE>" +
                                        "</Params>" +
                                    "</AddWOCost>" +
                                  "</Request>";

                    XmlDocument xmlReply = new XmlDocument();
                    xmlReply = CayentaPost(postData, "<AddWOCost>", cwWO.WORKORDERID);

                    //string something = new xmlReply.OuterXml;

                    //pick out batch and sheet from response
                    try
                    {

                        Console.WriteLine("XML REPLY was not null");
                        XmlNodeList Reply = xmlReply.SelectNodes("Reply");
                        XmlNodeList AddWorkOrderCost = Reply[0].SelectNodes("AddWOCost");
                        XmlNodeList Params = AddWorkOrderCost[0].SelectNodes("Params");
                        XmlNodeList STATUS = Params[0].SelectNodes("STATUS"); //DEE - request status
                        

                        if (STATUS[0].InnerText == "0")
                        {
                            string BATCH_NO = Params[0].SelectSingleNode("BATCH_NO").InnerText;
                            string SHEET_NO = Params[0].SelectSingleNode("SHEET_NO").InnerText;

                            foreach (CWCayentaXchange.EEF.LABORCOSTACT asst in laborcostact)
                            {
                                //DEE -- crew cards are by date
                                if (asst.STARTDATE == date)
                                {
                                    string earningCode = "0";
                                    if (asst.OCCUPATIONCODE != null) { earningCode = asst.OCCUPATIONCODE; }

                                    string resourceID = CityworksFunc.GetLabourResourceID(Convert.ToDecimal(asst.LABORSID));
                                    resourceList = resourceList +
                                                    "<RESOURCE><SEQ_NO></SEQ_NO><RESOURCE_TYPE>PA</RESOURCE_TYPE>" +
                                                    "<RESOURCE_ID>" + resourceID + "</RESOURCE_ID>" +
                                                    "<AMOUNT>" + asst.COST + "</AMOUNT>" +
                                                    "<QUANTITY>" + asst.HOURS + "</QUANTITY>" +
                                                    "<JOB_OBJECT>" + asst.ACCTNUM + "</JOB_OBJECT>" +
                                                    "<CODE_2>" + earningCode + "</CODE_2>" +
                                                    "</RESOURCE>";
                                }
                            }
                            foreach (CWCayentaXchange.EEF.EQUIPMENTCOSTACT asst in equipmentcostact)
                            {
                                if (asst.STARTDATE == date)
                                {
                                    resourceList = resourceList +
                                                   "<RESOURCE><SEQ_NO></SEQ_NO><RESOURCE_TYPE>EQ</RESOURCE_TYPE>" +
                                                   "<RESOURCE_ID>" + asst.EQUIPMENTUID + "</RESOURCE_ID>" +
                                                   "<QUANTITY>" + asst.HOURSREQUIRED + "</QUANTITY>" +
                                                   "<JOB_OBJECT>6550</JOB_OBJECT>" +
                                                   "</RESOURCE>";
                                }
                            }

                            //Putting it all together here:
                            postData = "<Request>" +
                                                "<SetWOCost>" +
                                                    "<Params>" +
                                                        "<PREFIX>" + wocategory + "</PREFIX>" +
                                                        "<ORDER_NUMBER>" + wonumber + "</ORDER_NUMBER>" +
                                                        "<ORDER_LINE_NO>1</ORDER_LINE_NO>" +
                                                        "<BATCH_NO>" + BATCH_NO + "</BATCH_NO>" +
                                                        "<SHEET_NO>" + SHEET_NO + "</SHEET_NO>" +
                                                        "<TRANS_DATE>" + startDate + "</TRANS_DATE>" +
                                                        "<ENTRY_SOURCE>V5</ENTRY_SOURCE><DESCRIPTION></DESCRIPTION>" +
                                                        "<RESOURCES>" +
                                                        resourceList
                                                        + "</RESOURCES>" +
                                                    "</Params>" +
                                                "</SetWOCost>" +
                                            "</Request>";


                            xmlCostReply = CayentaPost(postData, "<SetWOCost>", cwWO.WORKORDERID);

                            try
                            {
                                Console.WriteLine("XML COST REPLY was not null");
                                XmlNodeList Reply2 = xmlCostReply.SelectNodes("Reply");
                                XmlNodeList SetWorkOrderCost = Reply2[0].SelectNodes("SetWOCost");
                                XmlNodeList Params2 = SetWorkOrderCost[0].SelectNodes("Params");
                                XmlNodeList STATUS2 = Params2[0].SelectNodes("STATUS"); //DEE - request status
                                

                                if (STATUS2[0].InnerText == "0")
                                {
                                    Console.WriteLine("Set To Crew Card");
                                    UpdateCityworksActualResources(cwWO, 0, date, "("+ BATCH_NO + "-" + SHEET_NO + ")");
                                    updatesucessful = true;
                                }
                                else//<SetWoCost> Communication Okay but failed becuase of some other reason
                                {
                                    XmlNodeList STATUS_DESC2 = Params2[0].SelectNodes("STATUS_DESC"); //DEE - request status

                                    Console.WriteLine("<SetWOCost> Communication sucessfull but failed becuase of some other reason");
                                    CityworksFunc.LogError("<SetWOCost>", cwWO.WORKORDERID, STATUS_DESC2[0].InnerText, "", "");
                                    UpdateCityworksActualResources(cwWO, 1, date, STATUS_DESC2[0].InnerText);
                                    updatesucessful = true;
                                }

                            }
                            catch //<SetWOCost> Communication was null 
                            {
                                Console.WriteLine("<SetWOCost> Communication was null");
                                CityworksFunc.LogError("<SetWOCost>", cwWO.WORKORDERID, "Communication Error", "", "");
                                UpdateCityworksActualResources(cwWO, 1, date, "Communication Error");
                                updatesucessful = false;
                            }

                        }//<AddWOCost> Communication Okay but failed becuase of some other reason
                        else
                        {
                            XmlNodeList STATUS_DESC = Params[0].SelectNodes("STATUS_DESC"); //DEE - request status

                            Console.WriteLine("<AddWOCost> Communication Okay but failed becuase of some other reason");
                            CityworksFunc.LogError("<AddWOCost>", cwWO.WORKORDERID, STATUS_DESC[0].InnerText, "", "");
                            UpdateCityworksActualResources(cwWO, 1, date, STATUS_DESC[0].InnerText);
                            updatesucessful = false;
                        }
                        

                    }
                    catch //<AddWOCost> Communication was null
                    {
                        Console.WriteLine("<AddWOCost> Communication was null");
                        CityworksFunc.LogError("<AddWOCost>", cwWO.WORKORDERID, "Communication Error", "", "");
                        UpdateCityworksActualResources(cwWO, 1, date, "Communication Error");
                        updatesucessful = false;

                    }//else if addwodcost was unsucessfull


                }//for each date

            } //if actual labor or equipment

            return updatesucessful;
            
        }

        public static XmlDocument SetWorkRequestPriority(XmlDocument cayentaWO, EEF.WORKORDER cwWO)
        {
            XmlDocument xmlReply = new XmlDocument();

            XmlNodeList Reply = cayentaWO.SelectNodes("Reply");
            XmlNodeList AddWorkRequest = Reply[0].SelectNodes("AddWorkRequest");
            XmlNodeList STATUS = AddWorkRequest[0].SelectNodes("STATUS");
            XmlNodeList Params = AddWorkRequest[0].SelectNodes("Params");
            
            string WP_PREFIX = Params[0].SelectSingleNode("WP_PREFIX").InnerText;
            string WP_NUMBER = Params[0].SelectSingleNode("WP_NUMBER").InnerText;

            //Putting it all together here:
            string postData = "<Request>" +
                                "<SetWorkRequest>" +
                                    "<Params>" +
                                        "<PREFIX>" + WP_PREFIX + "</PREFIX>" +
                                        "<NUMBER>" + WP_NUMBER + "</NUMBER>" +
                                        "<PRIORITY>" + CityworksFunc.GetPriorityDesciption(cwWO.PRIORITY) + "</PRIORITY>" +
                                "</Params>" +
                            "</SetWorkRequest>" +
                          "</Request>";

            xmlReply = CayentaPost(postData, "<SetWorkRequest> Priority", cwWO.WORKORDERID);
            return xmlReply;
        }

        public static XmlDocument SetWorkRequestStatus(XmlDocument cayentaWO, EEF.WORKORDER cwWO)
        {
            XmlDocument xmlReply = new XmlDocument();

            XmlNodeList Reply = cayentaWO.SelectNodes("Reply");
            XmlNodeList AddWorkRequest = Reply[0].SelectNodes("AddWorkRequest");
            XmlNodeList STATUS = AddWorkRequest[0].SelectNodes("STATUS");
            XmlNodeList Params = AddWorkRequest[0].SelectNodes("Params");

            string WP_PREFIX = Params[0].SelectSingleNode("WP_PREFIX").InnerText;
            string WP_NUMBER = Params[0].SelectSingleNode("WP_NUMBER").InnerText;

            //Putting it all together here:
            string postData = "<Request>" +
                                "<SetWorkRequest>" +
                                    "<Params>" +
                                        "<PREFIX>" + WP_PREFIX + "</PREFIX>" +
                                        "<NUMBER>" + WP_NUMBER + "</NUMBER>" +
                                        "<STATUS>" + cwWO.STATUS + "</STATUS>" +
                                "</Params>" +
                            "</SetWorkRequest>" +
                          "</Request>";

            xmlReply = CayentaPost(postData, "<SetWorkRequest> Status", cwWO.WORKORDERID);
            return xmlReply;
        }

        public static XmlDocument SetWorkRequest(EEF.WORKORDER cwWO)
        {
            CredentialCache mycache = new CredentialCache();
            if (serviceUri == null) { serviceUri = new Uri(ConfigurationManager.AppSettings["CayentaAPIUri"]); }
            WebRequest webRequest = WebRequest.Create(serviceUri);
            mycache.Add(serviceUri, "Basic", new NetworkCredential(ConfigurationManager.AppSettings["CayentaUserID"], ConfigurationManager.AppSettings["CayentaLoginPwd"]));
            webRequest.Credentials = mycache;
            webRequest.Credentials.Equals("U0ZHOk1pbmltdW1mMHVydGVlbg==");
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "POST";

            
            string postData = "<Request>" +
                                "<SetWorkRequest>" +
                                    "<Params>" +
                                        "<PREFIX>" + cwWO.WOCATEGORY + "</PREFIX>" +
                                        "<NUMBER>" + cwWO.WORKORDERID + "</NUMBER>" +
                                        "<PRIORITY>" + CityworksFunc.GetPriorityDesciption(cwWO.PRIORITY) + "</PRIORITY>" +
                                        "<STATUS>" + cwWO.STATUS + "</STATUS>" +
                                "</Params>" +
                            "</SetWorkRequest>" +
                          "</Request>";

            XmlDocument xmlReply = new XmlDocument();
            xmlReply = CayentaPost(postData, "<SetWorkRequest>", cwWO.WORKORDERID);
            return xmlReply;
        }

        public static XmlDocument SetWorkOrder(EEF.WORKORDER cwWO)
        {
            CredentialCache mycache = new CredentialCache();
            if (serviceUri == null) { serviceUri = new Uri(ConfigurationManager.AppSettings["CayentaAPIUri"]); }
            WebRequest webRequest = WebRequest.Create(serviceUri);
            mycache.Add(serviceUri, "Basic", new NetworkCredential(ConfigurationManager.AppSettings["CayentaUserID"], ConfigurationManager.AppSettings["CayentaLoginPwd"]));
            webRequest.Credentials = mycache;
            webRequest.Credentials.Equals("U0ZHOk1pbmltdW1mMHVydGVlbg==");
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "POST";


            string postData = "<Request>" +
                                "<SetWorkOrder>" +
                                    "<Params>" +
                                        "<PREFIX>" + cwWO.WOCATEGORY + "</PREFIX>" +
                                        "<NUMBER>" + cwWO.WORKORDERID + "</NUMBER>" +
                                        "<ORDER_STATUS>" + cwWO.STATUS + "</ORDER_STATUS>" +
                                        "<PRIORITY>" + CityworksFunc.GetPriorityDesciption(cwWO.PRIORITY) + "</PRIORITY>" +
                                "</Params>" +
                            "</SetWorkOrder>" +
                          "</Request>";

            XmlDocument xmlReply = new XmlDocument();
            xmlReply = CayentaPost(postData, "<SetWorkOrder>", cwWO.WORKORDERID);
            return xmlReply;
        }

        public static void UpdateCityworks()
        {
            Console.WriteLine("UpdateCityworks");
        }

        public static void SyncEmployees(EEF.Cityworks_PUD_Entities cwEntities)
        {
            EEF.cayentaEntities entities = new EEF.cayentaEntities();
            foreach (EEF.CAY_CW_EMPLOYEE item in entities.CAY_CW_EMPLOYEE)
            {
                //get employee in cityworks
                EEF.EMPLOYEE cwEmployee = CWCayentaXchange.CityworksFunc.GetEmployeeByEmployeeId(item.employee_no.Trim());
                if (cwEmployee == null)
                {
                    //new record to add to cityworks
                    CWCayentaXchange.CityworksFunc.AddCityworksEmployee(cwEntities, item);
                }
                else
                {
                    //update it
                    CityworksFunc.UpdateCityworksEmployee(cwEntities, item);
                }
            }
            Console.WriteLine("SyncEmployees.  Save Status: " + cwEntities.SaveChanges());

            /*//This object is used to access the Views on DANU for the data sync
            EEF.cayentaEntities entities = new EEF.cayentaEntities();

            //Loops through each of the Cayenta Employee records
            foreach (EEF.CAY_CW_EMPLOYEE item in entities.CAY_CW_EMPLOYEE)
            {
                //Try to find the employee in Cityworks.
                EEF.EMPLOYEE cwEmployee = CWCayentaXchange.CityworksFunc.GetEmployeeByEmployeeId(item.employee_no.Trim());
                if (cwEmployee == null)
                {
                    //If the record is not in Cityworks, add it
                    CWCayentaXchange.CityworksFunc.AddCityworksEmployee(cwEntities, item);
                }
                else
                {
                    //If the record exists in Cityworks, add it if necessary
                    CityworksFunc.UpdateCityworksEmployee(cwEntities, item);
                }
            }
            //cwEntities.SaveChanges() returns the number of records affected by the method
            Console.WriteLine("SyncEmployees.  Save Status: " + cwEntities.SaveChanges());*/

        }

        /// <summary>
        /// Synchronizes Contractor records from Cayenta's database (DANU) to Cityworks.
        /// </summary>
        public static void SyncContractors(EEF.Cityworks_PUD_Entities cwEntities)
        {

            EEF.cayentaEntities entities = new EEF.cayentaEntities();
            foreach (EEF.CAY_CW_CONTRACTOR item in entities.CAY_CW_CONTRACTOR)
            {
                EEF.CONTRACTORLEAF cwContractorLeaf = CWCayentaXchange.CityworksFunc.GetContractorLeafByContractorNumber(item.contractor_no.Trim());
                if (cwContractorLeaf == null)
                {
                    CWCayentaXchange.CityworksFunc.AddCityworksContractor(cwEntities, item);
                }
                else
                {
                    CityworksFunc.UpdateCityworksContractor(cwEntities, item);
                }
            }
            Console.WriteLine("SyncContractor.  Save Status: " + cwEntities.SaveChanges());

            /*//This object is used to access the Views on DANU for the data sync
            EEF.cayentaEntities entities = new EEF.cayentaEntities();

            //Loops through each of the Cayenta Contractor records
            foreach (EEF.CAY_CW_CONTRACTOR item in entities.CAY_CW_CONTRACTOR)
            {
                //Try to find the contractor in Cityworks.
                EEF.CONTRACTORLEAF cwContractorLeaf = CWCayentaXchange.CityworksFunc.GetContractorLeafByContractorNumber(item.contractor_no.Trim());
                if (cwContractorLeaf == null)
                {
                    //If the record is not in Cityworks, add it
                    CWCayentaXchange.CityworksFunc.AddCityworksContractor(cwEntities, item);
                }
                else
                {
                    //If the record exists in Cityworks, add it if necessary
                    CityworksFunc.UpdateCityworksContractor(cwEntities, item);
                }
            }
            //cwEntities.SaveChanges() returns the number of records affected by the method
            Console.WriteLine("SyncContractor.  Save Status: " + cwEntities.SaveChanges());*/
        }

        /// <summary>
        /// Synchronizes Equipment records from Cayenta's database (DANU) to Cityworks.
        /// </summary>
        public static void SyncEquipment(EEF.Cityworks_PUD_Entities cwEntities)
        {
            EEF.cayentaEntities entities = new EEF.cayentaEntities();
            foreach (EEF.CAY_CW_EQUIPMENT item in entities.CAY_CW_EQUIPMENT)
            {
                EEF.EQUIPMENTLEAF cwEquipmentLeaf = CWCayentaXchange.CityworksFunc.GetEquipmentLeafByUId(item.EQUIPMENT_NO.Trim());
                if (cwEquipmentLeaf == null)
                {
                    CWCayentaXchange.CityworksFunc.AddCityworksEquipment(cwEntities, item);
                }
                else
                {
                    CityworksFunc.UpdateCityworksEquipment(cwEntities, item);
                }
            }
            Console.WriteLine("SyncEquipment.  Save Status: " + cwEntities.SaveChanges());

            /*//This object is used to access the Views on DANU for the data sync
            EEF.cayentaEntities entities = new EEF.cayentaEntities();

            //Loops through each of the Cayenta Inventory records
            foreach (EEF.CAY_CW_EQUIPMENT item in entities.CAY_CW_EQUIPMENT)
            {
                //Try to find the material in Cityworks.
                EEF.EQUIPMENTLEAF cwEquipmentLeaf = CWCayentaXchange.CityworksFunc.GetEquipmentLeafByUId(item.EQUIPMENT_NO.Trim());
                if (cwEquipmentLeaf == null)
                {
                    //If the record is not in Cityworks, add it
                    CWCayentaXchange.CityworksFunc.AddCityworksEquipment(cwEntities, item);
                }
                else
                {
                    //If the record exists in Cityworks, add it if necessary
                    CityworksFunc.UpdateCityworksEquipment(cwEntities, item);
                }
            }
            //cwEntities.SaveChanges() returns the number of records affected by the method
            Console.WriteLine("SyncEquipment.  Save Status: " + cwEntities.SaveChanges());*/
        }

        /// <summary>
        /// Synchronizes Materials (Inventory) records from Cayenta's database (DANU) to Cityworks.
        /// </summary>
        public static void SyncMaterials(EEF.Cityworks_PUD_Entities cwEntities)
        {
            EEF.cayentaEntities entities = new EEF.cayentaEntities();
            foreach (EEF.CAY_CW_INVENTORY item in entities.CAY_CW_INVENTORY)
            {
                EEF.MATERIALLEAF cwMaterialLeaf = CWCayentaXchange.CityworksFunc.GetMaterialLeafByUId(item.ITEM_NO.Trim());
                if (cwMaterialLeaf == null)
                {
                    CWCayentaXchange.CityworksFunc.AddCityworksMaterial(cwEntities, item);
                }
                else
                {
                    CityworksFunc.UpdateCityworksMaterial(cwEntities, item);
                }
            }
            Console.WriteLine("SyncMaterials.  Save Status: " + cwEntities.SaveChanges());

            /*//This object is used to access the Views on DANU for the data sync
            EEF.cayentaEntities entities = new EEF.cayentaEntities();

            //Loops through each of the Cayenta Inventory records
            foreach (EEF.CAY_CW_INVENTORY item in entities.CAY_CW_INVENTORY)
            {
                //Try to find the material in Cityworks.
                EEF.MATERIALLEAF cwMaterialLeaf = CWCayentaXchange.CityworksFunc.GetMaterialLeafByUId(item.ITEM_NO.Trim());
                if (cwMaterialLeaf == null)
                {
                    //If the record is not in Cityworks, add it
                    CWCayentaXchange.CityworksFunc.AddCityworksMaterial(cwEntities, item);
                }
                else
                {
                    //If the record exists in Cityworks, add it if necessary
                    CityworksFunc.UpdateCityworksMaterial(cwEntities, item);
                }
            }
            //cwEntities.SaveChanges() returns the number of records affected by the method
            Console.WriteLine("SyncMaterials.  Save Status: " + cwEntities.SaveChanges());*/
        }
    }
}
