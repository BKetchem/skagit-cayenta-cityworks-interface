﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CWCayentaXchange
{
    class Program
    {
        private static EEF.Cityworks_PUD_Entities cwEntities { get; set; }

        
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Help.showHelp(Help.HelpTopics.ALL);
                return;
            }
            else
            {
                if (cwEntities == null) { cwEntities = new EEF.Cityworks_PUD_Entities(); }

                switch (args[0])
                {
                    case "NSTOCWSO":
                        //North|Star to Cityworks
                        //Create Cayenta Work Order

                        //Close North|Star Service Order

                        break;
                    case "NSTOCWC":
                        //Customer Data Integration - Source: North|Star

                        break;
                    case "CWTOCAY":

                        //dee test
                        Console.WriteLine("CWTOCAY CALLED");

                        //all work orders not processed to Cayenta
                        //DEE where PROCESSEDTOCAYENTA = 0 and COMMENT!= 'INVALID JOB COST'
                        System.Linq.IQueryable<CWCayentaXchange.EEF.CINT_CAYENTA_STATUS> cityworksWork = CityworksFunc.GetCityworksWork();

                        //process new work orders
                        if (cityworksWork.Count() > 0)
                        {
                            bool updateTgWorkOrders = false;
                            XmlDocument cayentaWO = null;
                            foreach (CWCayentaXchange.EEF.CINT_CAYENTA_STATUS wo in cityworksWork)
                            {
                                IQueryable<EEF.WORKORDER> cwWO = cwEntities.WORKORDERs.Where(p => p.WORKORDERID == wo.WORKORDERID);
                                EEF.WORKORDER cwWorkOrder = cwWO.First();
                                
                                //create the work order in Cayenta <AddWorkRequest>
                                cayentaWO = Cayenta.AddWorkRequest(cwWorkOrder);

                                //if status ok from response
                                //DEE - checking for communication STATUS = 0 here
                                if (Cayenta.CayentaServiceSucess(cayentaWO)) //
                                {



                                    //update staging table to set processed to cayent flag as true
                                    //DEE - or updating ERROR column with error
                                   // updateTgWorkOrders = CityworksFunc.UpdateCINT_CAYENTA_STATUSS(cayentaWO, wo);

                                    if (updateTgWorkOrders)
                                    {
                                       //if staging table sucessfully updated set work order request and priority
                                        if (!string.IsNullOrEmpty(cwWorkOrder.PRIORITY))
                                        {
                                            //set priority to cayenta
                                            XmlDocument SetWOPriorityDoc = Cayenta.SetWorkRequestPriority(cayentaWO, cwWorkOrder);
                                            //set status to cayenta
                                            XmlDocument SetWOStatusDoc = Cayenta.SetWorkRequestStatus(cayentaWO, cwWorkOrder);

                                            //check for assets to add
                                            XmlDocument assetdoc = Cayenta.AddWorkRequestAsset(cwWorkOrder);

                                            //check for estimated labour, materials and equipment
                                            XmlDocument estdoc = Cayenta.AddWorkRequestEstimatedResources(cwWorkOrder);

                                            //check for actual
                                            XmlDocument actdoc = Cayenta.AddWorkRequestActualResources(cwWorkOrder);

                                            //no longer doing this separate program
                                            //bring back labor, equip, material, contractor from cayenta
                                            //Cayenta.getCayentaAssets(cwWorkOrder, cwEntities);
                                           
                                            //since adding assets, estimated resources and actual resources will trigger an update flag, reset to false because this is a new work order creation
                                            //and everythign is handled above, let triggers fire again if user re-enters something in this work order
                                            wo.UPDATETOCAY = false;
                                            //wo.UPDATEACTUALTOCAY = false;
                                            
                                        }
                                    }
                                    //DEE - Move this outside of if statement need to save changes that result in error
                                    try
                                    {
                                        //save the new values to the staging table contained in the wo object (row)
                                        cwEntities.CINT_CAYENTA_STATUSs.Attach(cwEntities.CINT_CAYENTA_STATUSs.Single(c => c.WORKORDERID == wo.WORKORDERID));
                                        cwEntities.CINT_CAYENTA_STATUSs.ApplyCurrentValues(wo);
                                        cwEntities.SaveChanges();
                                    }
                                    catch (Exception ex)
                                    {
                                        System.Diagnostics.Debug.WriteLine(ex.Message);
                                    }
                                }
                                //else
                            }

                        }
                                       

                        //all work orders processed to Cayenta but have update flag triggered
                        System.Linq.IQueryable<CWCayentaXchange.EEF.CINT_CAYENTA_STATUS> cityworkUpdate = CityworksFunc.GetCityworksUpdates();

                        //all work orders processed to Cayenta but have actuals update flag triggered
                        System.Linq.IQueryable<CWCayentaXchange.EEF.CINT_CAYENTA_STATUS> cityworkActUpdate = CityworksFunc.GetCityworksActualUpdates();

                        //DEE - removed for testing
                        /*
                        if (cityworkUpdate.Count() > 0)
                        {
                            foreach (CWCayentaXchange.EEF.CINT_CAYENTA_STATUS wo in cityworkUpdate)
                            {
                                IQueryable<EEF.WORKORDER> cwWo = cwEntities.WORKORDERs.Where(p => p.WORKORDERID == wo.WORKORDERID);
                                EEF.WORKORDER cwWorkOrder = cwWo.First();

                                //update work order status
                                XmlDocument cayentaWO = Cayenta.SetWorkOrder(cwWorkOrder);
                                bool updatesucessful = false;
                                if (Cayenta.CayentaWOUpdated(cayentaWO))
                                {
                                    updatesucessful = true;
                                }
                                else
                                {
                                    updatesucessful = false;
                                }
                                //update work request priority
                                XmlDocument cayentaWR = Cayenta.SetWorkRequest(cwWorkOrder);
                                if (Cayenta.CayentaWRUpdated(cayentaWR))
                                {
                                    updatesucessful = true;
                                }
                                else
                                {
                                    updatesucessful = false;
                                }
                                //check for assets to add
                                //DEE - Removed for testing
                                ////XmlDocument assetdoc = Cayenta.AddWorkRequestAsset(cwWorkOrder);

                                //check for estimated labour, materials and equipment
                                //DEE - Removed for testing
                                ////XmlDocument estdoc = Cayenta.AddWorkRequestEstimatedResources(cwWorkOrder);

                                //check for actual
                                //XmlDocument actdoc = Cayenta.AddWorkRequestActualResources(cwWorkOrder);


                                //no longer doing this - separate program
                                //bring back labor, equip, material, contractor from cayenta
                                //Cayenta.getCayentaAssets(cwWorkOrder, cwEntities);

                                //if sucessfull
                                //bool updatesucessful = true;
                                if (updatesucessful)
                                {
                                    //wo.PROCESSEDTOCAYENTA = true;
                                    //wo.PROCESSEDTOCAYENTADATE = DateTime.Now;
                                    //set as false meaning this has been updated and does not need to be updated, trigger takes care of flagging back to true when another update happens
                                    wo.UPDATETOCAY = false;
                                    //wo.UPDATEACTUALTOCAY = false;
     
                                    //committ the changes to staging
                                    try
                                    {
                                        cwEntities.CINT_CAYENTA_STATUSS.Attach(cwEntities.CINT_CAYENTA_STATUSS.Single(c => c.WORKORDERID == wo.WORKORDERID));
                                        cwEntities.CINT_CAYENTA_STATUSS.ApplyCurrentValues(wo);
                                        cwEntities.SaveChanges();
                                    }
                                    catch (Exception ex)
                                    {
                                        System.Diagnostics.Debug.WriteLine(ex.Message);
                                    }
                                }

                            }
                        } */ //DEE - Removed for testing...

                      
                        break;
                    case "CATOCW":

                        break;
                    case "SYNCALL":
                        //DEE - SYNCALL has been replaced with DB Links in SQL Server becuase of the addition of Projects
                        //Cayenta.SyncEmployees(cwEntities);
                        //Cayenta.SyncMaterials(cwEntities);
                        //Cayenta.SyncEquipment(cwEntities);
                        //Cayenta.SyncContractors(cwEntities);
                        break;
                    case "CATOCWEMP":
                        //Employee Data Integration - Source: Cayenta
                        //Cayenta.SyncEmployees(cwEntities);
                        break;
                    case "CATOCWMAT":
                        //Materials Data Integration - Source: Cayenta
                        //Cayenta.SyncMaterials(cwEntities);

                        break;
                    case "CATOCWEQIP":
                        //Equipment Data Integration - Source: Cayenta
                        //Cayenta.SyncEquipment(cwEntities);

                        break;
                    case "CATOCWCONT":
                        //Contractor Data Integration - Source: Cayenta
                        //Cayenta.SyncContractors(cwEntities);
                        break;
                    case "/?":
                        if (args.Length == 1)
                        {
                            Help.showHelp(Help.HelpTopics.ALL);
                        }
                        else if (args.Length == 2)
                        {
                            switch (args[1])
                            {
                                case "CATOCWCONT":
                                    Help.showHelp(Help.HelpTopics.CATOCWCONT);
                                    break;
                                case "CATOCWEMP":
                                    Help.showHelp(Help.HelpTopics.CATOCWEMP);
                                    break;
                                case "CATOCWEQIP":
                                    Help.showHelp(Help.HelpTopics.CATOCWEQIP);
                                    break;
                                case "CATOCWMAT":
                                    Help.showHelp(Help.HelpTopics.CATOCWMAT);
                                    break;
                                case "CATOCWWO":
                                    Help.showHelp(Help.HelpTopics.CATOCWWO);
                                    break;
                                case "CWTONS":
                                    Help.showHelp(Help.HelpTopics.CWTONS);
                                    break;
                                case "NSTOCWC":
                                    Help.showHelp(Help.HelpTopics.NSTOCWC);
                                    break;
                                case "NSTOCWSO":
                                    Help.showHelp(Help.HelpTopics.NSTOCWSO);
                                    break;
                            }
                        }
                        else { Help.showHelp(Help.HelpTopics.ALL); }


                        break;
                }
            }
        }
    }
}
