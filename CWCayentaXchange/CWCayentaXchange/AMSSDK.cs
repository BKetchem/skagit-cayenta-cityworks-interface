﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Cityworks.Core;

namespace CWCayentaXchange
{
    public static class AMSSDK
    {
        #region vars
        const string SESSION_SERVICE_FACTORY = "ServiceFactory";
        private static Uri serviceUri = new Uri(ConfigurationManager.AppSettings["serviceUri"]);
        private static CWUser CwUser { get; set; }
        private static Services factory { get; set; }
        private static EntityService.GroupsResponse entityGroups { get; set; }
        private static EntityService.TypesResponse entityTypes { get; set; }
        private static PWEntity pwEntity { get; set; }
        private static WorkOrderService.TemplatesResponse woTemplates { get; set; }
        private static WOTemplateName woTemplate { get; set; }
        private static WorkOrderService.TemplateCustomFieldsResponse woCustomFields { get; set; }
        private static MaterialService.AllResponse materials { get; set; }
        private static List<WorkOrderEntity> woEntities { get; set; }
        private static WorkOrderEntity woEntity { get; set; }
        #endregion
    }
}
