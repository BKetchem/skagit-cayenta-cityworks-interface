﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace CWCayentaXchange
{
    public static class Help
    {
        public enum HelpTopics
        {
            ALL,
            NSTOCWSO,
            NSTOCWC,
            CWTONS,
            CATOCWWO,
            CATOCWEMP,
            CATOCWMAT,
            CATOCWEQIP,
            CATOCWCONT
        }

        public static void showHelp(HelpTopics topic)
        {
            StringBuilder sb = new StringBuilder();
            switch (topic)
            {
                case HelpTopics.ALL:
                    sb.Append("\n\r\n\r");
                    sb.Append("");
                    sb.Append("\tCityworksIntegrator ? = This help screen\n\r");
                    sb.Append("\t[ NSTOCWSO = North|Star to Cityworks (Service Orders) ]\n\r");
                    sb.Append("\t[ NSTOCWC = North|Star to Cityworks (Customers) ]\n\r");
                    sb.Append("\t[ CWTONS = Cityworks to North|Star (Service Order Update) ]\n\r");
                    sb.Append("\t[ CATOCWWO = Cayenta to Cityworks (Work Order Update) ]\n\r");
                    sb.Append("\t[ CATOCWEMP = Cayenta to Cityworks (Employees) ]\n\r");
                    sb.Append("\t[ CATOCWMAT = Cayenta to Cityworks (Materials) ]\n\r");
                    sb.Append("\t[ CATOCWEQIP = Cayenta to Cityworks (Equipment) ]\n\r");
                    sb.Append("\t[ CATOCWCONT = Cayenta to Cityworks (Contractors) ]\n\r");
                    sb.Append("\n\r");
                    sb.Append("\tNOTE: Enter CityworksIntegrator ? /command for specifics\n\r");
                    sb.Append("\ton each from the above list\n\r\n\r");
                    Assembly assem = Assembly.GetEntryAssembly();
                    AssemblyName assemName = assem.GetName();
                    Version ver = assemName.Version;
                    sb.Append("\t" + assemName.Name + " " + ver.ToString());
                    sb.Append("\n\r");
                    sb.Append("\tBy Timmons Group, Richmond, VA 23225\n\r");
                    sb.Append("\thttp://www.timmons.com");

                    System.Console.WriteLine(sb.ToString());
                    break;
                case HelpTopics.CATOCWCONT:
                    sb.Append("\n\r\n\r");
                    sb.Append("");
                    sb.Append("\tCityworksIntegrator CATOCWCONT data,data,data\n\r");
                    break;
            }
        }
    }
}
