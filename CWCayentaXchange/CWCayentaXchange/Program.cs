﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CWCayentaXchange
{
    class Program
    {
        private static EEF.Cityworks_PUD_Entities cwEntities { get; set; }

        
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Help.showHelp(Help.HelpTopics.ALL);
                return;
            }
            else
            {
                if (cwEntities == null) { cwEntities = new EEF.Cityworks_PUD_Entities(); }

                switch (args[0])
                {
                    case "CWTOCAY":

                        //dee test
                        Console.WriteLine("CWTOCAY CALLED");
                        //Console.ReadLine();
                        
                        //all work orders not processed to Cayenta
                        System.Linq.IQueryable<CWCayentaXchange.EEF.CINT_CAYENTA_STATUS> cityworksWork = CityworksFunc.GetCityworksWork();
                        
                        //process new work orders
                        if (cityworksWork.Count() > 0)
                        {
                            
                            XmlDocument cayentaWO = null;

                            foreach (CWCayentaXchange.EEF.CINT_CAYENTA_STATUS wo in cityworksWork)
                            {
                                IQueryable<EEF.WORKORDER> cwWO = cwEntities.WORKORDERs.Where(p => p.WORKORDERID == wo.WORKORDERID);
                                EEF.WORKORDER cwWorkOrder = cwWO.First();
                                
                                //create the work order in Cayenta <AddWorkRequest>
                                cayentaWO = Cayenta.AddWorkRequest(cwWorkOrder);

                                CityworksFunc.UpdateCINT_CAYENTA_STATUS(cayentaWO, wo);

                                if (Cayenta.CayentaWOCreated(cayentaWO, cwWorkOrder.WORKORDERID))
                                {
                                        
                                    //set Priority //cannot be used for updating priority
                                    Cayenta.SetWorkRequestPriority(cayentaWO, cwWorkOrder); //Done

                                    //set status to cayenta
                                    Cayenta.SetWorkRequestStatus(cayentaWO, cwWorkOrder); //Done

                                    //check for assets to add
                                    Cayenta.AddWorkRequestAsset(cwWorkOrder);

                                    //check for estimated labour, materials and equipment
                                    Cayenta.AddWorkRequestEstimatedResources(cwWorkOrder);

                                    wo.UPDATETOCAY = false;
                                
                                    try
                                    {
                                        //save the new values to the staging table contained in the wo object (row)
                                        cwEntities.CINT_CAYENTA_STATUSs.Attach(cwEntities.CINT_CAYENTA_STATUSs.Single(c => c.WORKORDERID == wo.WORKORDERID));
                                        cwEntities.CINT_CAYENTA_STATUSs.ApplyCurrentValues(wo);
                                        cwEntities.SaveChanges();
                                    }
                                    catch (Exception ex)
                                    {
                                        System.Diagnostics.Debug.WriteLine(ex.Message);
                                    }

                                }//if CayentaWOCreated
                                
                            }//foreach

                        }//if cityworksWork
                                       

                        //all work orders processed to Cayenta but have update flag triggered
                        System.Linq.IQueryable<CWCayentaXchange.EEF.CINT_CAYENTA_STATUS> cityworkUpdate = CityworksFunc.GetCityworksUpdates();

                        if (cityworkUpdate.Count() > 0)
                        {
                            foreach (CWCayentaXchange.EEF.CINT_CAYENTA_STATUS wo in cityworkUpdate)
                            {
                                IQueryable<EEF.WORKORDER> cwWo = cwEntities.WORKORDERs.Where(p => p.WORKORDERID == wo.WORKORDERID);
                                EEF.WORKORDER cwWorkOrder = cwWo.First();

                                bool updatesucessful = true;

                                //check wo status is Cayenta is not CLOSED 
                                if (Cayenta.CayentaWOOpen(cwWorkOrder)) //return true the work order is still open
                                {
                                    
                                    //update work order status
                                    XmlDocument cayentaWO = Cayenta.SetWorkOrder(cwWorkOrder);

                                    

                                    if (Cayenta.CayentaWOUpdated(cayentaWO))
                                    {
                                        //do nothing
                                        //updatesucessful = true;
                                    }
                                    else
                                    {
                                        updatesucessful = false;
                                    }

                                    //update work request priority
                                    XmlDocument cayentaWR = Cayenta.SetWorkRequest(cwWorkOrder);

                                    if (Cayenta.CayentaWRUpdated(cayentaWR))
                                    {
                                        //do nothing
                                        //updatesucessful = true;
                                    }
                                    else
                                    {
                                        updatesucessful = false;
                                    }


                                    XmlDocument assetdoc = Cayenta.AddWorkRequestAsset(cwWorkOrder);


                                    XmlDocument estdoc = Cayenta.AddWorkRequestEstimatedResources(cwWorkOrder);
                                    
                                }//if the work order in canyenta is open  
 
                               


                                 //set as false meaning this has been updated and does not need to be updated, trigger takes care of flagging back to true when another update happens
                                 //update sucessfully swap
                                wo.UPDATETOCAY = !updatesucessful;

                                 //committ the changes to staging
                                try
                                {
                                    cwEntities.CINT_CAYENTA_STATUSs.Attach(cwEntities.CINT_CAYENTA_STATUSs.Single(c => c.WORKORDERID == wo.WORKORDERID));
                                    cwEntities.CINT_CAYENTA_STATUSs.ApplyCurrentValues(wo);
                                    cwEntities.SaveChanges();
                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Debug.WriteLine(ex.Message);
                                }
                                    
                               
                            }
                         
                        }


                        //all work orders processed to Cayenta but have actuals update flag triggered
                        System.Linq.IQueryable<CWCayentaXchange.EEF.CINT_CAYENTA_STATUS> cityworkActUpdate = CityworksFunc.GetCityworksActualUpdates();

                        if (cityworkActUpdate.Count() > 0)
                        {
                            foreach (CWCayentaXchange.EEF.CINT_CAYENTA_STATUS wo in cityworkActUpdate)
                            {
                                IQueryable<EEF.WORKORDER> cwWo = cwEntities.WORKORDERs.Where(p => p.WORKORDERID == wo.WORKORDERID);
                                EEF.WORKORDER cwWorkOrder = cwWo.First();

                                bool updatesucessful = true; 

                                Console.WriteLine("xxxxxxxxxxxxxxxxxxxxx");
                                //Console.ReadLine();

                                //check wo status is Cayenta to see if it is still open
                                if (Cayenta.CayentaWOOpen(cwWorkOrder)) //return true the work order is still open and continue
                                {

                                    //check for actual
                                    updatesucessful = Cayenta.AddWorkRequestActualResources(cwWorkOrder);
                                
                                }
                                   
                                // !updatesucessfull swap update sucessfull for needs to go to cayenta
                                wo.UPDATEACTUALTOCAY = !updatesucessful;

                                //committ the changes to staging
                                try
                                {
                                    cwEntities.CINT_CAYENTA_STATUSs.Attach(cwEntities.CINT_CAYENTA_STATUSs.Single(c => c.WORKORDERID == wo.WORKORDERID));
                                    cwEntities.CINT_CAYENTA_STATUSs.ApplyCurrentValues(wo);
                                    cwEntities.SaveChanges();
                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Debug.WriteLine(ex.Message);
                                }
                                

                            }

                        } 


                        break;

                    case "CAYTOCW":

                        //Get Cayenta Actuals and write them to Cityworks 
                        System.Linq.IQueryable<CWCayentaXchange.EEF.CINT_CAYENTA_STATUS> cityworksAct = CityworksFunc.GetCityworksNotClosed();

                        if (cityworksAct.Count() > 0)
                        {
                            foreach (CWCayentaXchange.EEF.CINT_CAYENTA_STATUS wo in cityworksAct)
                            {
                                IQueryable<EEF.WORKORDER> cwWO = cwEntities.WORKORDERs.Where(p => p.WORKORDERID == wo.WORKORDERID);
                                EEF.WORKORDER cwWorkOrder = cwWO.First();
                                Cayenta.getCayentaAssets(cwWorkOrder);

                                //closing the work order in get getCayentaAssets if need be.... 
                                cwEntities.WORKORDERs.ApplyCurrentValues(cwWorkOrder);
                                cwEntities.SaveChanges();
                            }
                        }

                        break;

                    case "SYNCALL":
                        //DEE - SYNCALL has been replaced with DB Links in SQL Server becuase of the addition of Projects
                        //Cayenta.SyncEmployees(cwEntities);
                        //Cayenta.SyncMaterials(cwEntities);
                        //Cayenta.SyncEquipment(cwEntities);
                        //Cayenta.SyncContractors(cwEntities);
                        break;
                    case "CATOCWEMP":
                        //Employee Data Integration - Source: Cayenta
                        //Cayenta.SyncEmployees(cwEntities);
                        break;
                    case "CATOCWMAT":
                        //Materials Data Integration - Source: Cayenta
                        //Cayenta.SyncMaterials(cwEntities);

                        break;
                    case "CATOCWEQIP":
                        //Equipment Data Integration - Source: Cayenta
                        //Cayenta.SyncEquipment(cwEntities);

                        break;
                    case "CATOCWCONT":
                        //Contractor Data Integration - Source: Cayenta
                        //Cayenta.SyncContractors(cwEntities);
                        break;
                    case "/?":
                        if (args.Length == 1)
                        {
                            Help.showHelp(Help.HelpTopics.ALL);
                        }
                        else if (args.Length == 2)
                        {
                            switch (args[1])
                            {
                                case "CATOCWCONT":
                                    Help.showHelp(Help.HelpTopics.CATOCWCONT);
                                    break;
                                case "CATOCWEMP":
                                    Help.showHelp(Help.HelpTopics.CATOCWEMP);
                                    break;
                                case "CATOCWEQIP":
                                    Help.showHelp(Help.HelpTopics.CATOCWEQIP);
                                    break;
                                case "CATOCWMAT":
                                    Help.showHelp(Help.HelpTopics.CATOCWMAT);
                                    break;
                                case "CATOCWWO":
                                    Help.showHelp(Help.HelpTopics.CATOCWWO);
                                    break;
                                case "CWTONS":
                                    Help.showHelp(Help.HelpTopics.CWTONS);
                                    break;
                                case "NSTOCWC":
                                    Help.showHelp(Help.HelpTopics.NSTOCWC);
                                    break;
                                case "NSTOCWSO":
                                    Help.showHelp(Help.HelpTopics.NSTOCWSO);
                                    break;
                            }
                        }
                        else { Help.showHelp(Help.HelpTopics.ALL); }


                        break;
                }
            }
        }
    }
}
